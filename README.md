這是學校專題《旅人》在 Bitbucket 上的倉庫。

=====開發目標=====

2016/1/15（進行中）
移除「庶民生活日記」的功能（「更多開放資料」的那一欄），那是多餘的。

2016/1/18
停 10 秒左右，顯示附近的資訊。
手動顯示 3 個離自己最近的資訊。

=====開發日誌=====

2016/1/14
終於傳成功了 Orz

2016/1/15
一直 Push 失敗，被提示說要先 Merge 之前的變更什麼的。後來發現前一天在 Bitbucket 網站上建立這個讀我檔，以致於兩端間的變更不同步。
後來照著 StackOverFlow 上的說法：先 Pull 後再重新 Push。果然就解決了！

2016/1/18
這一兩天在編譯這個應用程式時，遇到了沒處理過的的錯誤訊息：

```
#!Java Message

Error:Execution failed for task ':
app:dexDebug'.com.android.ide.common.process.ProcessException:
org.gradle.process.internal.ExecException:
Process 'command '/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/bin/java'' finished with non-zero exit value 2
```


上網查詢後發現是一個關於 Building Apps with Over 65K Methods 的問題。細節日後有空閒時會查明，這邊先列出參考網頁：

Maverick.pe，2015，〈Java finished with non-zero exit value 2 - Android Gradle〉，StackOverFlo。
http://stackoverflow.com/questions/29756188/java-finished-with-non-zero-exit-value-2-android-gradle

Android Developers，〈Building Apps with Over 65K Methods〉。
http://developer.android.com/intl/zh-tw/tools/building/multidex.html

後來依照網友的建議，在 build.gradle 內的 android／defaultConfig 區塊內加入：

```
#!Gradle

multiDexEnabled true
```

就能成功編譯了。

另外小生正在重新命名一些 Layout 檔的名字，好讓它們更好認出自己的功用。
然後真的該重新想想整個程式的流程了。

2016/1/19
加入「再按一次返回鍵結束程式」功能，取代之前的確認用對話方塊。

2016/2/3
在使用 Fragment 時，發現之前使用的 onAttach(Activity) 已經 deprecated 了，上 stackOverFlow 查詢後，網友表示可以
用 onAttach(Context) 代替。不過在實作時發現 onAttach(Context) 不知道為何不會被呼叫，而我特別寫出來把 Context 帶出，
給一些需要代入 Context 的方法，用的變數自然會是 null，造成程式無法執行。
後來有個解決方法是：

先在全域變數宣告區加入

```
#!Java

Activity activity;
```

後來在 onCreateView 區塊內加入

```
#!Java

activity = getActivity();
```

也就是說，我把以前用來帶 activity 變數的方法，換個寫法。
我覺那應該是個 bug ，有空閒時再上網查看後續消息。