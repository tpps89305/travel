package dispy.play.travel.photopicker;

import android.net.Uri;

/**
 * Created by yangchaofu on 2017/7/18.
 */

public interface OnHandlePickResultCallBack {
    void done(int resultCode, Uri resultUri);
}
