package dispy.play.travel.photopicker;

/**
 * 列舉相片的來源。
 * {@code ALBUM} 為相簿、{@code CAMERA} 為相機。
 *
 * @author Dispy
 * @version 1.1
 * @since 2017/8/22
 */
public enum PhotoSource {
    ALBUM, CAMERA
}
