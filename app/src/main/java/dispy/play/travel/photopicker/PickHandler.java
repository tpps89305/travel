package dispy.play.travel.photopicker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.yalantis.ucrop.UCrop;

import java.io.File;

import dispy.play.travel.main.MainActivity;

/**
 * 選取相片的功能。
 *
 * @author dispyTranslate
 * @version 1.1
 * @since 2017/8/9
 */
public class PickHandler {

    private final static int REQUEST_PICK_IMAGE = 1;
    private final static int REQUEST_PHOTO_IMAGE = 2;
    private static final int REQUEST_GOTO_CROP = 69;
    private static final int ACTION_DONE = 0;
    private static final int ACTION_CANCELED = -1;
    private static final int ACTION_ERROR = -2;
    private static final String DOWNLOAD_FOLDER_NAME = "/travel/image/";
    private static final String TMP_FILE_NAME = "photo.jpg";
    private static final String TMP_FILE_NAME2 = "photo_after.jpg";
    private final String TAG = this.getClass().getSimpleName();
    private Uri mPhotoUri;

    private Activity mActivity;
    private boolean mIsNeedCrop;
    private OnHandlePickResultCallBack mListener;
    private PhotoSource mSource = PhotoSource.ALBUM;
    private TakePurpose mPurpose = TakePurpose.PURPOSE_USER_PROFILE;

    public PickHandler(Activity activity) {
        mActivity = activity;
    }

    public PickHandler setIsNeedCrop(boolean isNeedCrop) {
        mIsNeedCrop = isNeedCrop;
        return this;
    }

    public PickHandler setPhotoSource(PhotoSource source) {
        mSource = source;
        return this;
    }

    /**
     * 設定相片的用途。
     *
     * @param takePurpose 用途，會決定要剪裁的比例。
     * @since 1.1
     **/
    public PickHandler setPropose(TakePurpose takePurpose) {
        mPurpose = takePurpose;
        return this;
    }

    public PickHandler pick(OnHandlePickResultCallBack listener) {
        mListener = listener;
        switch (mSource) {
            case ALBUM:
                makePhotoFile(TMP_FILE_NAME);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                mActivity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
                break;
            case CAMERA:
                mPhotoUri = makePhotoUri(makePhotoFile(TMP_FILE_NAME));
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                mActivity.startActivityForResult(cameraIntent, REQUEST_PHOTO_IMAGE);
                break;
        }
        return this;
    }

    private File makePhotoFile(String fileName) {
        File filePath = Environment.getExternalStoragePublicDirectory(DOWNLOAD_FOLDER_NAME);
        if (filePath.mkdirs()) {
            Log.i(TAG, "建立了相片資料夾");
        } else {
            Log.w(TAG, "建立相片資料夾時發生錯誤");
        }
        File mPhotoFile = new File(filePath, fileName);
        Log.d(TAG, "存放相片的地方: " + mPhotoFile.toString());
        return mPhotoFile;
    }

    private Uri makePhotoUri(File photoFile) {
        return FileProvider.getUriForFile(mActivity,
                mActivity.getApplicationContext().getPackageName() + ".provider", photoFile); //Android 7 對於檔案存取權限的新方式
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri uri;
        if (resultCode == MainActivity.RESULT_OK) {
            switch (requestCode) {

                case REQUEST_PICK_IMAGE:
                    uri = data.getData();
                    if (mIsNeedCrop) {
                        File photoFile = makePhotoFile(TMP_FILE_NAME);
                        if (mPhotoUri == null)
                            mPhotoUri = Uri.fromFile(photoFile);
                        gotoUCrop(uri, mPhotoUri);
                    } else {
                        mListener.done(ACTION_DONE, uri);
                    }
                    break;

                case REQUEST_PHOTO_IMAGE:
                    File photoFile2 = makePhotoFile(TMP_FILE_NAME2);
                    Uri uriAfter = Uri.fromFile(photoFile2);
                    if (mIsNeedCrop) {
                        gotoUCrop(mPhotoUri, uriAfter);
                    } else {
                        mListener.done(ACTION_DONE, mPhotoUri);
                    }
                    break;

                case REQUEST_GOTO_CROP:
                    Log.d(TAG, "從剪裁程式回來了");
                    final Uri resultUri = UCrop.getOutput(data);
                    mListener.done(ACTION_DONE, resultUri);
                    break;
            }
        } else {
            if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                Log.e(TAG, "uCrop 錯誤: " + cropError);
                mListener.done(ACTION_ERROR, null);
            } else {
                mListener.done(ACTION_CANCELED, null);
            }

        }
    }

    private void gotoUCrop(Uri originUri, Uri editUri) {
        if (mPurpose == TakePurpose.PURPOSE_USER_PROFILE) {
            UCrop.of(originUri, editUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(512, 512)
                    .start(mActivity);
        } else if (mPurpose == TakePurpose.PURPOSE_ARTICLE) {
            UCrop.of(originUri, editUri)
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(1920, 1080)
                    .start(mActivity);
        }
    }

}
