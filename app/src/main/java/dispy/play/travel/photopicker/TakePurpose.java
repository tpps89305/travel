package dispy.play.travel.photopicker;

/**
 * Created by yangchaofu on 2017/8/25.
 */

public enum TakePurpose {
    PURPOSE_USER_PROFILE, PURPOSE_ARTICLE
}
