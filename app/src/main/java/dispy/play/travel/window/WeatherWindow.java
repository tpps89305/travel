package dispy.play.travel.window;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.net.NetworkHandler;
import dispy.play.travel.reference.Parameter;
import dispy.play.travel.R;
import dispy.play.travel.adapter.WeatherListAdapter;

/**
 * Created by tpps8 on 2016/4/25.
 */
public class WeatherWindow {

    private Context context;
    private PopupWindow popupWindow;
    private ListView listView;

    private Bitmap[] arrayWeatherIcon;
    private List<HashMap<String, String>> list;

    private Handler handler = new Handler() {
        public void handleMessage (Message msg) {
            switch (msg.what) {
                case Parameter.REFRESH_DATA:
                    listView.setAdapter(new WeatherListAdapter(context, list, arrayWeatherIcon));
                    break;
            }
        }
    };

    public WeatherWindow (Context context) {
        this.context = context;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.window_weather_detail, null);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (int) (size.x * 0.9);
        int height = (int) (size.y * 0.9);

        popupWindow = new PopupWindow(view, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        LinearLayout mainWindow = view.findViewById(R.id.layout_window);
        RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(width,height);
        parms.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainWindow.setLayoutParams(parms);
        popupWindow.setBackgroundDrawable(new ColorDrawable());

        listView = view.findViewById(R.id.list_view);
    }

    public void showWindow(View view, final List<HashMap<String, String>> list) {

        this.list = list;

        new Thread() {
            public void run() {
                arrayWeatherIcon = new Bitmap[list.size()];
                for (int i = 0; i < arrayWeatherIcon.length; i++) {
                    try {
                        arrayWeatherIcon[i] = NetworkHandler.getWeatherIcon(list.get(i).get("icon"));
                        handler.sendEmptyMessage(Parameter.REFRESH_DATA);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        final View pvu = popupWindow.getContentView();

        pvu.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK)
                {
                    popupWindow.dismiss();
                }
                return false;
            }
        });
    }
}
