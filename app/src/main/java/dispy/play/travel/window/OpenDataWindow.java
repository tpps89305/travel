package dispy.play.travel.window;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.object.FoodData;
import dispy.play.travel.object.HistoricalSitesData;
import dispy.play.travel.reference.DataTag;
import dispy.play.travel.R;
import dispy.play.travel.adapter.OpenDataListAdapter;
import dispy.play.travel.main.Search;
import dispy.play.travel.main.ShowRoute;

/**
 * 檢視 OpenData 詳細內容的視窗
 * 另有搜尋、導航按鈕引導使用相關功能
 */
public class OpenDataWindow implements View.OnClickListener {

    @BindView(R.id.list_view)
    ListView listView;
    @BindView(R.id.btn_route)
    ImageButton buttonRoute;
    @BindView(R.id.btn_search)
    ImageButton buttonSearch;
    @BindView(R.id.text_title)
    TextView textTitle;
    @BindView(R.id.text_address)
    TextView textSubTitle;

    private PopupWindow popupWindow;
    private Context context;

    private String[] foodDataTags = {
            "title",
            "address",
            "business_hrs",
            "description",
            "phone",
            "price",
            "route",
            "modify_date"
    };

    private String[] cultureDataTags = {
            "title",
            "content",
            "time",
            "locationName",
            "address",
            "price"
    };

    private String[] historicalSitesTags = {
            "title",
            "address",
            "level",
            "buildingYear",
            "longitude",
            "latitude",
            "srcWebsite"
    };

    private double latitude, longitude;
    private String title, address, url;
    private DataTag current;

    public OpenDataWindow (Context context) {
        this.context = context;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.window_opendata_detail, null);

        ButterKnife.bind(this, view);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = (int) (size.x * 0.9);
        int height = (int) (size.y * 0.9);

        popupWindow = new PopupWindow(view, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
        LinearLayout mainWindow = view.findViewById(R.id.layout_window);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width,height);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainWindow.setLayoutParams(params);
        popupWindow.setBackgroundDrawable(new ColorDrawable());

        buttonRoute.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);
    }

    public void showFoodData(View view, FoodData foodData) {
        title = foodData.getTitle();
        try {
            latitude = foodData.getLatitude();
        } catch (NumberFormatException e) {
            latitude = -1;
        }
        try {
            longitude = foodData.getLongitude();
        } catch (NumberFormatException e) {
            longitude = -1;
        }
        textTitle.setText(foodData.getTitle());
        textSubTitle.setText(foodData.getAddress());
        List<HashMap<String, String>> toDisplay = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("title", "營業時間");
        hashMap.put("content", foodData.getBusinessHours());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "描述");
        hashMap.put("content", foodData.getDescription());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "電話");
        hashMap.put("content", foodData.getPhone());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "價錢");
        hashMap.put("content", foodData.getPrice());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "營業時間");
        hashMap.put("content", foodData.getBusinessHours());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "交通方式");
        hashMap.put("content", foodData.getRoute());
        toDisplay.add(hashMap);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        OpenDataListAdapter adapter = new OpenDataListAdapter(context, toDisplay, R.layout.style_food_list_item);
        listView.setAdapter(adapter);

        final View pvu = popupWindow.getContentView();
        pvu.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK)
                {
                    popupWindow.dismiss();
                }
                return false;
            }
        });
    }

    public void showHSData(View view, HistoricalSitesData data) {
        title = data.getName();
        latitude = data.getLatitude();
        longitude = data.getLongitude();
        textTitle.setText(data.getName());
        textSubTitle.setText(data.getAddress());

        List<HashMap<String, String>> toDisplay = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("title", "名稱");
        hashMap.put("content", data.getName());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "地址");
        hashMap.put("content", data.getAddress());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "級別");
        hashMap.put("content", data.getLevel());
        toDisplay.add(hashMap);
        hashMap = new HashMap<>();
        hashMap.put("title", "創建年代");
        hashMap.put("content", data.getBuildingYear());
        toDisplay.add(hashMap);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        OpenDataListAdapter adapter = new OpenDataListAdapter(context, toDisplay, R.layout.style_food_list_item);
        listView.setAdapter(adapter);

        final View pvu = popupWindow.getContentView();
        pvu.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK)
                {
                    popupWindow.dismiss();
                }
                return false;
            }
        });
    }

    public void showWindow(View view, HashMap data, DataTag tag) {
        current = tag;
        List<HashMap<String, String>> toDisplay = new ArrayList<>();

        title = data.get("title").toString();
        try {
            latitude = Double.parseDouble(data.get("latitude").toString());
        } catch (NumberFormatException e) {
            latitude = -1;
        }
        try {
            longitude = Double.parseDouble(data.get("longitude").toString());
        } catch (NumberFormatException e) {
            longitude = -1;
        }
        String[] itemTitles;

        switch (tag) {
            case FOOD:
                itemTitles = context.getResources().getStringArray(R.array.array_food);
                textTitle.setText(data.get(foodDataTags[0]).toString());
                textSubTitle.setText(data.get(foodDataTags[1]).toString());
                address = data.get(foodDataTags[1]).toString();
                for (int i = 2; i < foodDataTags.length; i++) {
                    if (!data.get(foodDataTags[i]).equals("")) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("title", itemTitles[i]);
                        hashMap.put("address", data.get(foodDataTags[i]).toString());
                        toDisplay.add(hashMap);
                    }
                }
                break;
            case CULTURE:
                itemTitles = context.getResources().getStringArray(R.array.array_culture);
                textTitle.setText(data.get(cultureDataTags[0]).toString());
                textSubTitle.setText(data.get(cultureDataTags[3]).toString());
                address = data.get(cultureDataTags[3]).toString();
                for (int i = 1; i < cultureDataTags.length; i++) {
                    if (!data.get(cultureDataTags[i]).equals("")) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("title", itemTitles[i]);
                        hashMap.put("address", data.get(cultureDataTags[i]).toString());
                        toDisplay.add(hashMap);
                    }
                }
                break;
            case HISTORICAL_SITES:
                itemTitles = context.getResources().getStringArray(R.array.array_historical_sites);
                textTitle.setText(data.get(historicalSitesTags[0]).toString());
                textSubTitle.setText(data.get(historicalSitesTags[1]).toString());
                address = data.get(historicalSitesTags[1]).toString();
                url = data.get("srcWebsite").toString();
                for (int i = 2; i < 4; i++) {
                    if (!data.get(historicalSitesTags[i]).equals("")) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("title", itemTitles[i]);
                        hashMap.put("address", data.get(historicalSitesTags[i]).toString());
                        toDisplay.add(hashMap);
                    }
                }
                break;
        }

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        OpenDataListAdapter adapter = new OpenDataListAdapter(context, toDisplay, R.layout.style_food_list_item);
        listView.setAdapter(adapter);

        final View pvu = popupWindow.getContentView();

        pvu.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK)
                {
                    popupWindow.dismiss();
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {

        Intent intent;
        Bundle bundle;

        switch (v.getId()) {
            case R.id.btn_search:
                bundle = new Bundle();
                bundle.putString("title", title);
                intent = new Intent(context, Search.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

//                if (current == DataTag.HISTORICAL_SITES) {
//                    bundle = new Bundle();
//                    bundle.putString("url", url);
//                    intent = new Intent(context, BoCHPage.class);
//                    intent.putExtras(bundle);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//                }
//                else {
//                    bundle = new Bundle();
//                    bundle.putString("title", title);
//                    intent = new Intent(context, Search.class);
//                    intent.putExtras(bundle);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//                }
                break;
            case R.id.btn_route:
                if (!(latitude == -1 || longitude == -1)) {
                    intent = new Intent(context, ShowRoute.class);
                    bundle = new Bundle();
                    bundle.putDouble("lat", latitude);
                    bundle.putDouble("lng", longitude);
                    bundle.putString("title", title);
                    bundle.putString("address", address);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                else {
                    Toast.makeText(context, "此地點 / 活動沒有定位資料", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

}
