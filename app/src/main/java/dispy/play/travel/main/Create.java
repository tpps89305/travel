package dispy.play.travel.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.StringHandler;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.map.Localization;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.photopicker.OnHandlePickResultCallBack;
import dispy.play.travel.photopicker.PhotoSource;
import dispy.play.travel.photopicker.PickHandler;
import dispy.play.travel.photopicker.TakePurpose;

/**
 * 建立新貼文。
 *
 * @author dispyTranslate
 * @version 1.1
 * @since 2017-08-30
 **/
public class Create extends BaseActivity {

    @BindView(R.id.edit_content)
    EditText editContent;
    @BindView(R.id.image_photo)
    ImageView imageView;
    @BindView(R.id.text_tip_add_image)
    TextView textTipAddImage;

    private PickHandler pickHandler;
    private Create mActivity;
    private Uri photoUri;
    private LoadingBox loadingBox;

    private FirebaseHandler firebaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        mActivity = this;
        ButterKnife.bind(mActivity);
        pickHandler = new PickHandler(mActivity);
        loadingBox = new LoadingBox(mActivity);
        firebaseHandler = FirebaseHandler.getInstance();

        setToolbar(R.string.action_title, R.mipmap.ic_keyboard_backspace_white_24dp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pickHandler.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            askQuit();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                askQuit();
                break;
            case R.id.action_submit:
                if (editContent.getText().toString().equals("")) {
                    showToast(R.string.toast_please_provide_content);
                } else if (photoUri == null) {
                    showToast(R.string.toast_please_provide_photo);
                } else {
                    uploadImage();
                }

                break;
        }
        return false;
    }

    private void askQuit() {
        AlertDialog.Builder ad = new AlertDialog.Builder(this); //創建訊息方塊
        ad.setTitle(R.string.dialog_ask_quit);
        ad.setMessage(R.string.dialog_ask_quit_content);
        ad.setPositiveButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() { //按"是",則退出應用程式

            public void onClick(DialogInterface dialog, int i) {
                finish();
            }

        });
        ad.setNegativeButton(R.string.dialog_button_no, null);
        ad.show();//顯示訊息視窗
    }

    @OnClick(R.id.image_photo)
    void takePicture() {
        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        } else {
            choosePhotoSource();
        }
    }

    private void choosePhotoSource() {
        String[] arrayList = getResources().getStringArray(R.array.dialog_choose_photo_method);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_choose_photo_method);
        builder.setItems(arrayList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PhotoSource source = PhotoSource.ALBUM;
                TakePurpose purpose = TakePurpose.PURPOSE_ARTICLE;
                switch (i) {
                    case 0:
                        source = PhotoSource.ALBUM;
                        break;
                    case 1:
                        source = PhotoSource.CAMERA;
                        break;
                }
                pickHandler = new PickHandler(mActivity)
                        .setIsNeedCrop(true)
                        .setPropose(purpose)
                        .setPhotoSource(source)
                        .pick(new OnHandlePickResultCallBack() {
                    @Override
                    public void done(int resultCode, Uri resultUri) {
                        if (resultCode == ACTION_DONE) {
                            Log.d(TAG, "回傳的相片 Uri: " + resultUri.getPath());
                            imageView.setImageURI(resultUri);
                            photoUri = resultUri;
                            textTipAddImage.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
        builder.show();
    }

    private void uploadText(String photoName, String idDate) {
        LatLng userPosition = Localization.getMyLocation(this);
        Address address = Localization.getAddress(this, userPosition);

        //取得要上傳的資料
        String content = editContent.getText().toString();
        double latitude = userPosition.latitude;
        double longitude = userPosition.longitude;
        String countryName = address.getCountryName();
        String adminArea = address.getAdminArea();
        String locality = address.getLocality();
        String addressLine = address.getAddressLine(0);

        String reference = "article/" + "/" + firebaseHandler.getUid() + "_" + idDate + "/";

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //內文
        DatabaseReference myRef = database.getReference(reference + "content");
        myRef.setValue(content);
        //緯度
        myRef = database.getReference(reference + "latitude");
        myRef.setValue(latitude);
        //經度
        myRef = database.getReference(reference + "longitude");
        myRef.setValue(longitude);
        //地址
        myRef = database.getReference(reference + "addressLine");
        myRef.setValue(addressLine);
        //相片網址
        myRef = database.getReference(reference + "photo");
        myRef.setValue(photoName);
        //發表日期
        myRef = database.getReference(reference + "date");
        myRef.setValue(StringHandler.getCurrentDate(StringHandler.WITH_SPLIT));
        //作者的 Uid
        myRef = database.getReference(reference + "uid");
        myRef.setValue(firebaseHandler.getUid());
        //作者的 Display Name
        myRef = database.getReference(reference + "displayName");
        myRef.setValue(firebaseHandler.getDisplayName());
        //CountryName
        myRef = database.getReference(reference + "countryName");
        myRef.setValue(countryName);
        //AdminArea
        myRef = database.getReference(reference + "adminArea");
        myRef.setValue(adminArea);
        //Locality
        myRef = database.getReference(reference + "locality");
        myRef.setValue(locality);

        myRef = database.getReference(reference + "adminArea_locality");
        myRef.setValue(adminArea + locality);

    }

    private void uploadImage() {
        loadingBox.show();
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance().getReference();
        final String idDate = StringHandler.getCurrentDate(StringHandler.FOR_ID_TAG);
        final String photoName = firebaseHandler.getUid() + "_" + idDate + ".jpg";
        StorageReference riversRef = storageRef.child("article/" + photoName);
        riversRef.putFile(photoUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        uploadText(photoName, idDate);
                        loadingBox.dismiss();
                        showToast(R.string.toast_create_done);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Log.w(TAG, "上傳圖片時發生錯誤");
                        showToast(R.string.toast_error_upload_photo);
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    choosePhotoSource();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "使用者拒絕給予權限");
                }
                break;
        }
    }
}
