package dispy.play.travel.main;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;
import dispy.play.travel.adapter.RouteListAdapter;
import dispy.play.travel.map.Localization;
import dispy.play.travel.map.MapHandler;
import dispy.play.travel.net.NetworkHandler;

/**
 * 地點導航畫面
 *
 * @author Dispy
 * @since 2015/9/2
 * @version 1.0
 */
public class ShowRoute extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    @BindView(R.id.list_view)
    ListView listView;

    private LatLng dataLatLng, userLatLng;
    private String title, content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_route);

        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        title = bundle.getString("title");
        content = bundle.getString("address");

        setToolbar(title, R.mipmap.ic_keyboard_backspace_white_24dp);

        dataLatLng = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lng"));
        userLatLng = Localization.getMyLocation(ShowRoute.this);

        SupportMapFragment fragment = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map));
        fragment.getMapAsync(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Google MapHandler 初始化
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 12.0f), 2000, null);

        MarkerOptions markerOpt;
        markerOpt = new MarkerOptions();
        markerOpt.position(userLatLng)
                .title("我現在在這裡！")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        mMap.addMarker(markerOpt);
        markerOpt = new MarkerOptions();
        markerOpt.position(dataLatLng)
                .title(title)
                .snippet(content)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mMap.addMarker(markerOpt);

        NetworkHandler.getDirectionsJSON(userLatLng, dataLatLng,
                new NetworkHandler.onReceivedMapDataListener() {
                    @Override
                    public void done(JSONObject result) {
                        MapHandler.drawPath(mMap, result);
                        try {
                            List<HashMap<String, String>> steps = MapHandler.getStep(result);
                            listView.setAdapter(new RouteListAdapter(ShowRoute.this, steps));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
