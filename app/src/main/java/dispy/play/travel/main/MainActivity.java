package dispy.play.travel.main;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import dispy.play.travel.component.FragmentInterface;
import dispy.play.travel.R;
import dispy.play.travel.fragment.DataList;
import dispy.play.travel.fragment.DataMap;
import dispy.play.travel.fragment.InformationPage;
import dispy.play.travel.map.Localization;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.net.FirebaseUserInfo;

/**
 * 這邊是起始畫面，一開始會進來的進方。
 */
public class MainActivity extends BaseActivity implements FragmentInterface, NavigationView.OnNavigationItemSelectedListener {

    private TextView navDisplayName;
    private TextView navEmail;
    private ImageView imagePhoto;

    private static final int REQUEST_SIGN = 0x001;
    private static boolean isExit = false;
    private static boolean hasTask = false;
    public Fragment fragment;
    Timer timeExit = new Timer();
    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            isExit = false;
            hasTask = true;
        }
    };
    //畫面布局
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    //自訂 Toolbar 會用到
    private ActionBar actionBar;

    private InformationPage informationPage;
    private DataMap dataMap;
    private DataList dataList;

    public static final int REQUEST_FROM_INFO_PAGE = 0x01;
    public static final int REQUEST_FROM_DATA_MAP = 0x02;
    public static final int REQUEST_FROM_DATA_LIST = 0x04;

    private FirebaseUserInfo firebaseUserInfo;
    private FirebaseUserInfo.UserInfoUpdateListener infoUpdateListener = new FirebaseUserInfo.UserInfoUpdateListener() {
        @Override
        public void onGetInfo(String displayName, String email, Uri photoUrl) {
            navDisplayName.setText(displayName);
            navEmail.setText(email);
            Picasso.with(getApplicationContext()).load(photoUrl).into(imagePhoto);
        }

        @Override
        public void onNoUser() {
            navDisplayName.setText(R.string.nav_no_login_name);
            navEmail.setText(R.string.toast_please_login);
            imagePhoto.setImageBitmap(null);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseUserInfo = FirebaseUserInfo.getInstance();
        firebaseUserInfo.setListener(infoUpdateListener);
        Localization.getInstance(this);

        //Toolbar 初始化
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setHomeAsUpIndicator(R.mipmap.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        initDrawerNavigation(toolbar);

        informationPage = new InformationPage();
        dataList = new DataList();
        dataMap = new DataMap();

        selectFragment();
        firebaseUserInfo.initFireBaseAth();
    }

    /**
     * 側邊導覽列初始化
     *
     * @param toolbar 搭配使用的 Toolbar
     **/
    private void initDrawerNavigation(Toolbar toolbar) {
        //控制項初始化
        drawerLayout = findViewById(R.id.drawer_layout);
        //側邊導覽列初始化
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close
        );
        drawerToggle.syncState();
        drawerLayout.addDrawerListener(drawerToggle);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);
        navDisplayName = view.findViewById(R.id.nav_display_name);
        navEmail = view.findViewById(R.id.nav_email);
        imagePhoto = view.findViewById(R.id.imageView);
    }

    @Override
    public void goToDataList(int dataTag) {
        String title; //Toolbar 標題

        fragment = new DataList();
        Bundle bundle = new Bundle();
        bundle.putInt("data_tag", dataTag);
        fragment.setArguments(bundle);
        title = "Toolbar 標題";
        actionBar.setTitle(title);

        // 更新被選擇項目，換標題文字，關閉選單
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SIGN:
                if (resultCode == RESULT_OK) {
                    firebaseUserInfo.getLoginInfo();
                }
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        fragment = new InformationPage();
        int id = item.getItemId();
        switch (id) {
            case R.id.main:
                fragment = informationPage;
                actionBar.setTitle(R.string.toolbar_information);
                break;
            case R.id.view_map:
                fragment = dataMap;
                actionBar.setTitle(R.string.toolbar_map);
                break;
            case R.id.view_list:
                fragment = dataList;
                actionBar.setTitle(R.string.toolbar_list);
                break;
            case R.id.new_post:
                if (firebaseUserInfo.hasUser()) {
                    startActivity(new Intent(this, Create.class));
                } else {
                    showToast(R.string.toast_please_login);
                    startActivity(new Intent(this, SignIn.class));
                }
                break;
            case R.id.sign:
                if (firebaseUserInfo.hasUser()) {
                    startActivity(new Intent(this, UserProfile.class));
                } else {
                    startActivity(new Intent(this, SignIn.class));
                }
                break;
        }
        // 更新被選擇項目，換標題文字，關閉選單
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * 選擇第一個分頁
     **/
    private void selectFragment() {
        fragment = new InformationPage();
        actionBar.setTitle(R.string.toolbar_information);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseUserInfo.addAuthStateListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        firebaseUserInfo.removeAuthStateListener();
    }

    //按下下方導覽列時，提示離開應用程式。
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!isExit) {
                isExit = true;
                showToast(R.string.toast_click_again_to_exit);
                if (!hasTask) {
                    timeExit.schedule(task, 2000);
                }
            } else {
                finish();
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_DATA_MAP:
                dataMap.onRequestPermissionsResult(permissions, grantResults);
                break;
            case Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_INFO_PAGE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    selectFragment();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "使用者拒絕給予權限");
                }
                break;
            case Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_DATA_LIST:
                dataList.onRequestPermissionsResult(permissions, grantResults);
                break;
        }
    }
}
