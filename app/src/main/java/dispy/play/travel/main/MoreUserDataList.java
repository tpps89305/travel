package dispy.play.travel.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.reference.Parameter;
import dispy.play.travel.R;
import dispy.play.travel.adapter.OpenDataListAdapter;
import dispy.play.travel.window.OpenDataWindow;

/**
 * Created by tpps8 on 2016/4/18.
 */
public class MoreUserDataList extends AppCompatActivity {

    private ListView listView;
    private List<HashMap<String, String>> userData;
    private String category = "";
    private List<HashMap<String, String>> toDisplayUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.window_weather_detail);

        listView = findViewById(R.id.list_view);

        Bundle bundle = getIntent().getExtras();
        category = bundle.getString("category");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("title", toDisplayUserData.get(position).get("title"));
                bundle.putString("content", toDisplayUserData.get(position).get("content"));
                bundle.putString("image_name", toDisplayUserData.get(position).get("image_name"));
                bundle.putString("post_no", toDisplayUserData.get(position).get("post_no"));
                bundle.putString("post_account_no", toDisplayUserData.get(position).get("post_account_no"));

                Intent intent = new Intent(MoreUserDataList.this, UserDataDetail.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });


    }

}
