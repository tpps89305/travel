package dispy.play.travel.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.StringHandler;
import dispy.play.travel.adapter.CommentListAdapter;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.net.FirebaseUserInfo;

/**
 * 留言功能，包括觀看與上傳留言等功能。
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017-8-29
 */

public class CommentActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private String postId;

    private FirebaseHandler firebaseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        ButterKnife.bind(this);
        firebaseHandler = FirebaseHandler.getInstance();

        Intent intent = getIntent();
        postId = intent.getStringExtra("postId");

        setToolbar(intent.getStringExtra("title"), R.mipmap.ic_keyboard_backspace_white_24dp);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        getComment(postId);
    }

    private void getComment(String postId) {

        firebaseHandler.getComment(postId, new FirebaseHandler.ArticleListener() {
            @Override
            public void done(List<HashMap<String, Object>> listArticle) {
                recyclerView.setAdapter(new CommentListAdapter(CommentActivity.this, listArticle));
            }

            @Override
            public void onFailure() {
                Log.e(TAG, "取得留言時發生錯誤");
            }
        });

    }

    @OnClick(R.id.fab_add)
    void openInputDialog() {
        if (FirebaseUserInfo.getInstance().hasUser()) {
            LayoutInflater adbInflater = LayoutInflater.from(this);
            final View progressLayout = adbInflater.inflate(R.layout.dialog_input_comment, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(progressLayout);
            builder.setTitle(R.string.dialog_upload_comment);
            builder.setPositiveButton(R.string.btn_submit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    EditText editInput = progressLayout.findViewById(R.id.edit_comment);
                    firebaseHandler.addComment(postId, editInput.getText().toString());
                }
            });
            builder.show();
        } else {
            startActivity(new Intent(this, SignIn.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
