package dispy.play.travel.main;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.Toast;

import dispy.play.travel.R;

/**
 * {@code Activity} 共用設定
 *
 * @author Dispy
 * @since 2017-8-22
 * @version 1.0
 */

public abstract class BaseActivity extends AppCompatActivity {

    public static final int ACTION_DONE = 0;
    public static final int ACTION_CANCELED = -1;
    public static final int ACTION_ERROR = -2;
    public static final int REQUEST_STORAGE = 0x01;
    public final String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //改變導覽列顏色
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.primary_dark));
        }
    }

    /**
     * 顯示 Toast 訊息
     *
     * @param message 訊息
     **/
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 顯示 Toast 訊息
     *
     * @param strId 字串 ResourceID
     **/
    public void showToast(@StringRes int strId) {
        Toast.makeText(this, strId, Toast.LENGTH_SHORT).show();
    }

    public Toolbar setToolbar(@StringRes int titleId, @DrawableRes int indicator) {
        return setToolbar(getResources().getString(titleId), indicator);
    }

    public Toolbar setToolbar(String title, @DrawableRes int indicator) {
        //Toolbar 初始化
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(title);
        actionBar.setHomeAsUpIndicator(indicator);
        actionBar.setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    public void setToolbar(@StringRes int titleId) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(getResources().getString(titleId));
    }

    public TextInputLayout getTextInputLayout(@NonNull EditText editText) {
        View currentView = editText;
        for (int i = 0; i < 2; i++) {
            ViewParent parent = currentView.getParent();
            if (parent instanceof TextInputLayout) {
                return (TextInputLayout) parent;
            } else {
                currentView = (View) parent;
            }
        }
        return null;
    }

}
