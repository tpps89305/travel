package dispy.play.travel.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.net.FirebaseHandler;

/**
 * Created by yangchaofu on 2017/8/22.
 */

public class SignUp extends BaseActivity {

    @BindView(R.id.edit_email)
    EditText editAccount;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.edit_password_again)
    EditText editPasswordAgain;

    TextInputLayout editAccountLayout;
    TextInputLayout editPasswordLayout;

    private LoadingBox loadingBox;
    private FirebaseHandler firebaseHandler;

    private TextWatcher watchEditAccount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().contains("@")) {
                editAccountLayout.setError(getResources().getString(R.string.toast_email_error));
            } else {
                editAccountLayout.setErrorEnabled(false);
            }
        }
    };
    private TextWatcher watchEditPassword = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().length() < 6) {
                editPasswordLayout.setError(getResources().getString(R.string.toast_password_length_error));
            } else {
                editPasswordLayout.setErrorEnabled(false);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sign_up);

        ButterKnife.bind(this);
        setToolbar(R.string.actionbar_sign_up);

        firebaseHandler = FirebaseHandler.getInstance();
        loadingBox = new LoadingBox(this);
        editAccount.addTextChangedListener(watchEditAccount);
        editAccountLayout = getTextInputLayout(editAccount);
        editPassword.addTextChangedListener(watchEditPassword);
        editPasswordLayout = getTextInputLayout(editPassword);
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        String email = editAccount.getText().toString();
        String password = editPassword.getText().toString();
        String passwordAgain = editPasswordAgain.getText().toString();

        if (!(email.contains("@") && password.length() >= 6))
            showToast(R.string.toast_please_input_current_info);
        else if (!password.equals(passwordAgain)) {
            showToast(R.string.toast_password_not_same);
        }
        else {
            submitData(email, password);
        }
    }

    private void submitData(String email, String password) {
        loadingBox.show();
        firebaseHandler.createAccountByEmail(this, email, password, new FirebaseHandler.UpdateDataListener() {
            @Override
            public void onSuccess() {
                showToast(R.string.toast_sign_up_success);
                startActivity(new Intent(SignUp.this, AddUserInfo.class));
                finish();
            }

            @Override
            public void onFailure() {
                showToast(R.string.toast_sign_up_failed);
            }
        });
    }

}
