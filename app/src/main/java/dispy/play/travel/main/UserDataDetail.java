package dispy.play.travel.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.object.Article;

/**
 * 檢視使用者自行上傳的資料
 * 包含觀看圖片、文章、按「讚」、留言功能
 *
 * @author dispyTranslate
 * @since 2017-09-07
 */
public class UserDataDetail extends BaseActivity {

    @BindView(R.id.text_content)
    TextView textContent;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.image_user_photo)
    ImageView imageUserPhoto;
    @BindView(R.id.text_user_display_name)
    TextView textUserDisplayName;

    private String postId;

    private FirebaseHandler firebaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data_detail);

        ButterKnife.bind(this);
        firebaseHandler = FirebaseHandler.getInstance();

        Article article = (Article)getIntent().getSerializableExtra("value");
        postId = article.getPostId();

        textContent.setText(article.getContent());
        firebaseHandler.getArticlePhoto(article.getPhoto(), image);
        firebaseHandler.getAuthorInfo(article.getUid(), new FirebaseHandler.GetAuthorInfoListener() {

            @Override
            public void done(String displayName, String userPhotoUrl) {
                textUserDisplayName.setText(displayName);
            }
        });
        firebaseHandler.getAuthorPhoto(article.getUid(), imageUserPhoto);

    }

    @OnClick(R.id.fab_comment)
    public void gotoComment() {
        Intent intent = new Intent(this, CommentActivity.class);
        intent.putExtra("title", "留言");
        intent.putExtra("postId", postId);
        startActivity(intent);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return true;
    }

}
