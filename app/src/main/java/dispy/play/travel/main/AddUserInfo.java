package dispy.play.travel.main;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.StringHandler;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.photopicker.OnHandlePickResultCallBack;
import dispy.play.travel.photopicker.PhotoSource;
import dispy.play.travel.photopicker.PickHandler;

/**
 * 在註冊帳號後，新增使用者的資料。
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017-08-22
 */

public class AddUserInfo extends BaseActivity {

    @BindView(R.id.edit_display_name)
    EditText editDisplayName;
    @BindView(R.id.btn_birthday)
    Button btnBirthday;
    @BindView(R.id.txt_introduce)
    EditText editIntroduce;

    private FirebaseHandler firebaseHandler;
    private LoadingBox loadingBox;
    private AddUserInfo mActivity;
    private PickHandler pickHandler;

    private Uri photoUri;

    private int mYear, mMonth, mDay;
    private boolean isDateSelected = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_info);

        ButterKnife.bind(this);

        setToolbar(R.string.toolbar_add_user_info);

        mActivity = this;
        pickHandler = new PickHandler(mActivity);
        firebaseHandler = FirebaseHandler.getInstance();
        loadingBox = new LoadingBox(this);
    }

    private void updateData() {
        loadingBox.show();
        firebaseHandler.addUserProfileInfo(photoUri, editDisplayName.getText().toString()
                , btnBirthday.getText().toString(), editIntroduce.getText().toString()
                , new FirebaseHandler.UpdateDataListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "User profile updated.");
                        loadingBox.dismiss();
                        showToast(R.string.toast_sign_up_done);
                        finish();
                    }

                    @Override
                    public void onFailure() {

                    }
                });
    }

    @OnClick(R.id.image_user)
    void takePicture() {
        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        } else {
            choosePhotoSource();
        }
    }

    void choosePhotoSource() {
        String[] arrayList = getResources().getStringArray(R.array.dialog_choose_photo_method);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_choose_photo_method);
        builder.setItems(arrayList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PhotoSource source = PhotoSource.ALBUM;
                switch (i) {
                    case 0:
                        source = PhotoSource.ALBUM;
                        break;
                    case 1:
                        source = PhotoSource.CAMERA;
                        break;
                }
                pickHandler = new PickHandler(mActivity)
                        .setIsNeedCrop(true)
                        .setPhotoSource(source)
                        .pick(new OnHandlePickResultCallBack() {
                            @Override
                            public void done(int resultCode, Uri resultUri) {
                                if (resultCode == ACTION_DONE) {
                                    Log.d(TAG, "Result Uri: " + resultUri.getPath());
                                    photoUri = resultUri;
                                }
                            }
                        });
            }
        });
        builder.show();
    }

    @OnClick(R.id.btn_birthday)
    void chooseBirthday() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                mYear = year;
                mMonth = month;
                mDay = day;
                btnBirthday.setText(StringHandler.toDateFormat(year, month, day));
                isDateSelected = true;
            }

        }, mYear, mMonth, mDay);
        datePickerDialog.updateDate(2000, 0, 1);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        if (editDisplayName.getText().toString().equals(""))
            showToast(R.string.toast_please_input_name);
        else if (!isDateSelected)
            showToast(R.string.toast_please_input_birthday);
        else
            updateData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pickHandler.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    choosePhotoSource();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "使用者拒絕給予權限");
                }
                break;
        }
    }

    //按下下方導覽列時，提示離開應用程式。
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showToast(R.string.toast_no_exit);
        }
        return false;
    }
}
