package dispy.play.travel.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.net.FirebaseHandler;

/**
 * Created by tpps8 on 2016/3/16.
 */
public class SignIn extends BaseActivity {

    @BindView(R.id.edit_account)
    EditText editAccount;
    @BindView(R.id.edit_password)
    EditText editPassword;
    TextInputLayout editAccountLayout;
    TextInputLayout editPasswordLayout;
    private LoadingBox loadingBox;
    private FirebaseHandler firebaseHandler;
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;
    private TextWatcher watchEditAccount = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().contains("@")) {
                editAccountLayout.setError(getResources().getString(R.string.toast_email_error));
            } else {
                editAccountLayout.setErrorEnabled(false);
            }
        }
    };
    private TextWatcher watchEditPassword = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.toString().length() < 6) {
                editPasswordLayout.setError(getResources().getString(R.string.toast_password_length_error));
            } else {
                editPasswordLayout.setErrorEnabled(false);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);
        firebaseHandler = FirebaseHandler.getInstance();

        mAuth = FirebaseAuth.getInstance();

        //Toolbar 初始化
        setToolbar(R.string.btn_login);

        loadingBox = new LoadingBox(this);

        initializeFacebook();

        editAccount.addTextChangedListener(watchEditAccount);
        editAccountLayout = getTextInputLayout(editAccount);
        editPassword.addTextChangedListener(watchEditPassword);
        editPasswordLayout = getTextInputLayout(editPassword);
    }

    private void initializeFacebook() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.btn_facebook_login);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");

            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);

            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        loadingBox.show();
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            showToast(R.string.toast_credential_success);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            showToast(R.string.toast_credential_success);
                        }
                        loadingBox.dismiss();
                    }
                });
    }

    @OnClick(R.id.button_sign_in)
    public void signIn() {
        String account = editAccount.getText().toString();
        String password = editPassword.getText().toString();

        if (account.contains("@") && password.length() >= 6) {
            loadingBox.show();
            firebaseHandler.signInByEmail(this, account, password, new FirebaseHandler.UpdateDataListener() {
                @Override
                public void onSuccess() {
                    loadingBox.dismiss();
                    Log.d(TAG, "登入成功");
                    showToast(R.string.toast_login_success);
                    finish();
                }

                @Override
                public void onFailure() {
                    loadingBox.dismiss();
                    Log.w(TAG, "登入失敗");
                    showToast(R.string.toast_login_failed);
                }
            });
        } else {
            if (!account.contains("@")) {
                editAccountLayout.setError(getResources().getString(R.string.toast_email_error));
            } else {
                editAccountLayout.setErrorEnabled(false);
            }
            if (password.length() < 6) {
                editPasswordLayout.setError(getResources().getString(R.string.toast_password_length_error));
            } else {
                editPasswordLayout.setErrorEnabled(false);
            }
        }

    }

    @OnClick(R.id.button_sign_up)
    public void signUp() {
        startActivity(new Intent(this, SignUp.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
