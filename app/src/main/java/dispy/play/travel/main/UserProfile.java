package dispy.play.travel.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.adapter.UserInfoAdapter;
import dispy.play.travel.component.EditDisplayNameBox;
import dispy.play.travel.component.EditIntroduceBox;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.component.OnDialogDismissListener;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.net.FirebaseHandler.UpdateDataListener;
import dispy.play.travel.net.FirebaseHandler.UpdateProfilePhotoListener;
import dispy.play.travel.photopicker.OnHandlePickResultCallBack;
import dispy.play.travel.photopicker.PhotoSource;
import dispy.play.travel.photopicker.PickHandler;

/**
 * 使用者的個人資訊頁
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017-08-21
 */
public class UserProfile extends BaseActivity {

    @BindView(R.id.image_user)
    ImageView imageUser;
    @BindView(R.id.list_info)
    ListView listInfo;
    private UserProfile mActivity;
    private PickHandler pickHandler;
    private LoadingBox loadingBox;
    private EditIntroduceBox editIntroduceBox;
    private FirebaseHandler firebaseHandler;

    private String mIntroduce;

    private static final int REQUEST_STORAGE = 0x01;

    private OnDialogDismissListener updateIntroduce = new OnDialogDismissListener() {
        @Override
        public void onClickPositiveButton(String introduce) {
            loadingBox.show();
            firebaseHandler.updateUserIntroduce(introduce);
            loadingBox.dismiss();
        }
    };
    private UpdateProfilePhotoListener updateProfilePhoto = new UpdateProfilePhotoListener() {
        @Override
        public void onSuccess() {
            loadingBox.dismiss();
            showToast(R.string.toast_changed);
            showUserPhoto(getApplicationContext(), imageUser);
        }

        @Override
        public void onFailure(Exception exception) {
            loadingBox.dismiss();
            showToast(R.string.toast_error_upload_photo);
        }
    };
    private UpdateDataListener updateDisplayName = new UpdateDataListener() {
        @Override
        public void onSuccess() {
            showToast(R.string.toast_changed);
            getUserData();
        }

        @Override
        public void onFailure() {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);
        mActivity = this;
        loadingBox = new LoadingBox(this);
        editIntroduceBox = new EditIntroduceBox(this);
        firebaseHandler = FirebaseHandler.getInstance();

        setToolbar(R.string.toolbar_user_profile, R.mipmap.ic_keyboard_backspace_white_24dp);

        showUserPhoto(this, imageUser);

        getUserData();

    }

    private void getUserData() {

        firebaseHandler.getUserInfo(new FirebaseHandler.GetUserInfoListener() {
            @Override
            public void done(List<HashMap<String, String>> userInfo, String birthday, String introduce) {
                mIntroduce = introduce;
                UserInfoAdapter userInfoAdapter = new UserInfoAdapter(getApplicationContext(), userInfo);
                listInfo.setAdapter(userInfoAdapter);
                listInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                new EditDisplayNameBox(UserProfile.this).show(updateDisplayName);
                                break;
                            case 5:
                                editIntroduceBox.show(mIntroduce, updateIntroduce);
                                break;
                        }
                    }
                });
            }
        });
    }

    @OnClick(R.id.image_user)
    void takePicture() {
        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        } else {
            choosePhotoSource();
        }
    }

    void choosePhotoSource() {
        String[] arrayList = getResources().getStringArray(R.array.dialog_choose_photo_method);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_choose_photo_method);
        builder.setItems(arrayList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PhotoSource source = PhotoSource.ALBUM;
                switch (i) {
                    case 0:
                        source = PhotoSource.ALBUM;
                        break;
                    case 1:
                        source = PhotoSource.CAMERA;
                        break;
                }
                pickHandler = new PickHandler(mActivity)
                        .setIsNeedCrop(true)
                        .setPhotoSource(source)
                        .pick(new OnHandlePickResultCallBack() {
                            @Override
                            public void done(int resultCode, Uri resultUri) {
                                if (resultCode == ACTION_DONE) {
                                    Log.d(TAG, "回傳的 Uri: " + resultUri.getPath());
                                    loadingBox.show();
                                    firebaseHandler.updateProfilePhoto(resultUri, updateProfilePhoto);
                                }
                            }
                        });
            }
        });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_user_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                firebaseHandler.signOut();
                finish();
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pickHandler.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    choosePhotoSource();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "使用者拒絕給予權限");
                }
                break;
        }
    }


    /**
     * 顯示使用者頭相
     *
     * @param context 相依的環境
     * @param toShow  要用來顯示頭相的 ImageView
     **/
    public void showUserPhoto(Context context, ImageView toShow) {
        try {
            Log.d(TAG, "showUserPhoto: " + firebaseHandler.getPhotoUrl());
            Picasso.with(context).load(firebaseHandler.getPhotoUrl()).error(R.drawable.ic_image_load_error).into(toShow);
        } catch (NullPointerException e) {
            Log.w(TAG, "沒有使用者頭相，或下載圖片時發生錯誤。");
        }
    }
}
