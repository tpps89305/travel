package dispy.play.travel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 下拉式選單樣式
 *
 * @author Dispy
 * @since 2017-9-15
 * @version 1.0
 */

public class SpinnerTagAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private String[] mArrayDataTag;

    public SpinnerTagAdapter (Context context, String[] arrayDataTag) {
        mInflater = LayoutInflater.from(context);
        mArrayDataTag = arrayDataTag;
    }

    @Override
    public int getCount() {
        return mArrayDataTag.length;
    }

    @Override
    public Object getItem(int i) {
        return mArrayDataTag[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = mInflater.inflate(R.layout.style_spinner_data_tag, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        holder.textCategory.setText((String) getItem(i));
        return view;
    }

    class ViewHolder {
        @BindView(R.id.text_category)
        TextView textCategory;
        ViewHolder (View view) {
            ButterKnife.bind(this, view);
        }
    }
}
