package dispy.play.travel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 自訂留言清單樣式。
 *
 * @author Dispy
 * @since 2017-9-15
 * @version 1.0
 */

public class CommentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HashMap<String, Object>> mCommentList;
    private Context mContext;

    public CommentListAdapter(Context context, List<HashMap<String, Object>> commentList) {
        mContext = context;
        mCommentList = commentList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerViewHolder recyclerViewHolder = (RecyclerViewHolder) holder;
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_recycler_item_show);
        recyclerViewHolder.mItemView.startAnimation(animation);
        recyclerViewHolder.textContent.setText(mCommentList.get(position).get("content").toString());
        recyclerViewHolder.textDate.setText(mCommentList.get(position).get("date").toString());
        recyclerViewHolder.textName.setText(mCommentList.get(position).get("displayName").toString());
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        View mItemView;
        @BindView(R.id.text_content)
        TextView textContent;
        @BindView(R.id.text_date)
        TextView textDate;
        @BindView(R.id.text_name)
        TextView textName;

        private RecyclerViewHolder (View view) {
            super(view);
            mItemView = view;
            ButterKnife.bind(this, view);
        }
    }
}
