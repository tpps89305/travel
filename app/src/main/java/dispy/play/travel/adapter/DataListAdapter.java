package dispy.play.travel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 資料列表樣式
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017/9/14
 */

public class DataListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private List<HashMap<String, String>> mItems;
    private Context mContext;
    private OnItemClickedListener listener;

    public DataListAdapter(Context context, List<HashMap<String, String>> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public void onClick(View view) {
        listener.onItemClicked(view, (int) view.getTag());
    }

    /**
     * 自行處理 Recycler View 的點擊事件
     *
     * @param listener 專用偵測器
     **/
    public void setListener(final OnItemClickedListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerViewHolder recyclerViewHolder = (RecyclerViewHolder) holder;
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_recycler_item_show);
        recyclerViewHolder.mItemView.startAnimation(animation);
        recyclerViewHolder.textTitle.setText(mItems.get(position).get("title"));
        recyclerViewHolder.textContent.setText(mItems.get(position).get("content"));
        recyclerViewHolder.mItemView.setOnClickListener(this);
        recyclerViewHolder.mItemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClickedListener {
        void onItemClicked(View view, int position);
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        View mItemView;
        @BindView(R.id.text_title)
        TextView textTitle;
        @BindView(R.id.text_content)
        TextView textContent;

        private RecyclerViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;
            ButterKnife.bind(this, itemView);
        }

    }
}
