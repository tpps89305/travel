package dispy.play.travel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 使用者資訊清單樣式
 *
 * @author Dispy
 * @since 2017-8-21
 * @version 1.0
 */

public class UserInfoAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<HashMap<String, String>> mUserData;

    public UserInfoAdapter(Context context, List<HashMap<String, String>> userData) {
        mInflater = LayoutInflater.from(context);
        mUserData = userData;
    }

    @Override
    public int getCount() {
        return mUserData.size();
    }

    @Override
    public Object getItem(int i) {
        return mUserData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = mInflater.inflate(R.layout.item_user_info, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        HashMap<String, String> hashMap = (HashMap<String, String>) getItem(i);
        holder.textTitle.setText(hashMap.get("title"));
        holder.textContent.setText(hashMap.get("content"));
        return view;
    }

    class ViewHolder {
        @BindView(R.id.text_title)
        TextView textTitle;
        @BindView(R.id.text_content)
        TextView textContent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
