package dispy.play.travel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 天氣清單樣式
 *
 * @author Dispy
 * @version 1.0
 * @since 2016-4-19
 */
public class WeatherListAdapter extends BaseAdapter {

    private List<HashMap<String, String>> list;
    private LayoutInflater inflater;
    private Bitmap[] arrayWeatherIcon;

    public WeatherListAdapter(Context context, List<HashMap<String, String>> list, Bitmap[] arrayWeatherIcon) {
        inflater = LayoutInflater.from(context);
        this.list = list;
        this.arrayWeatherIcon = arrayWeatherIcon;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.style_weather_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textTime.setText(list.get(position).get("date"));
        holder.textWeather.setText(list.get(position).get("weather"));
        holder.textTemp.setText(list.get(position).get("temperature"));
        holder.imageView.setImageBitmap(arrayWeatherIcon[position]);
        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.weather)
        TextView textWeather;
        @BindView(R.id.time)
        TextView textTime;
        @BindView(R.id.temp)
        TextView textTemp;
        @BindView(R.id.icon)
        ImageView imageView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
