package dispy.play.travel.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 自訂資料清單樣式
 *
 * @author Dispy
 * @since 2015-8-31
 * @version 1.0
 */
public class OpenDataListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<HashMap<String, String>> foodItem;
    private int itemStyle;

    public OpenDataListAdapter(Context context, List<HashMap<String, String>> foodItem, int itemStyle) {
        this.inflater = LayoutInflater.from(context);
        this.foodItem = foodItem;
        this.itemStyle = itemStyle;
    }

    @Override
    public int getCount() {
        return foodItem.size();
    }

    @Override
    public Object getItem(int position) {
        return foodItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(itemStyle, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }
        holder.textTitle.setText(foodItem.get(position).get("title"));
        holder.textContent.setText(foodItem.get(position).get("content"));
        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.text_title)
        TextView textTitle;
        @BindView(R.id.text_content)
        TextView textContent;

        ViewHolder (View view) {
            ButterKnife.bind(this, view);
        }
    }

}
