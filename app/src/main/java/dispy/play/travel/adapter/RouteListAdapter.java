package dispy.play.travel.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * 自訂路徑清單樣式
 *
 * @author Dispy
 * @since 2015-9-2
 * @version 1.0
 */
public class RouteListAdapter extends BaseAdapter {

    private List<HashMap<String, String>> foodData;
    private LayoutInflater inflater;

    public RouteListAdapter (Context context, List<HashMap<String, String>> foodData) {
        inflater = LayoutInflater.from(context);
        this.foodData = foodData;
    }

    @Override
    public int getCount() {
        return foodData.size();
    }

    @Override
    public Object getItem(int position) {
        return foodData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.style_route_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        HashMap<String, String> temp = (HashMap<String, String>) getItem(position);
        holder.textDistance.setText(temp.get("distance"));
        holder.textTime.setText(temp.get("duration"));
        holder.textStep.setText(Html.fromHtml(temp.get("html_instructions")));
        return convertView;
    }

    class ViewHolder{
        @BindView(R.id.text_distance)
        TextView textDistance;
        @BindView(R.id.text_time)
        TextView textTime;
        @BindView(R.id.text_step)
        TextView textStep;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
