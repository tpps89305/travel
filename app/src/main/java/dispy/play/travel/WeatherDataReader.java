package dispy.play.travel;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.Stack;

import dispy.play.travel.object.Weather;

public class WeatherDataReader extends XMLParsingHandler {

    private static final String TAG = "WeatherDataReader";
    private String city = "";
    private String date = "";
    private Weather wdItem;
    private Stack<Weather> mItem_list;

    public WeatherDataReader() {
    }

    @Override
    public Object getParsedData() {
        Weather[] arr_wdItem = mItem_list.toArray(new Weather[mItem_list.size()]);
        Log.v(TAG, String.format("Weather Data = %d", arr_wdItem.length));
        return new Object[]{arr_wdItem};
    }

    public void startDocument() throws SAXException {
        super.startDocument();
        mItem_list = new Stack<>();
    }

    public void endDocument() throws SAXException {
        super.endDocument();
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        super.startElement(namespaceURI, localName, qName, atts);
        if (getInNode().size() >= 5
                && getInNode().get(getInNode().size() - 5).equals("cwbopendata")
                && getInNode().get(getInNode().size() - 4).equals("dataset")
                && getInNode().get(getInNode().size() - 3).equals("location")
                && getInNode().get(getInNode().size() - 2).equals("weatherElement")
                && getInNode().get(getInNode().size() - 1).equals("time")) {
            wdItem = new Weather();
        }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (getInNode().size() >= 5
                && getInNode().get(getInNode().size() - 5).equals("cwbopendata")
                && getInNode().get(getInNode().size() - 4).equals("dataset")
                && getInNode().get(getInNode().size() - 3).equals("location")
                && getInNode().get(getInNode().size() - 2).equals("weatherElement")
                && getInNode().get(getInNode().size() - 1).equals("time")) {
            mItem_list.add(wdItem);
            wdItem = null;
        }
        super.endElement(namespaceURI, localName, qName);
    }

    public void characters(String fetchStr) {

        if (getInNode().size() == 4
                && getInNode().get(getInNode().size() - 4).equals("cwbopendata")
                && getInNode().get(getInNode().size() - 3).equals("dataset")
                && getInNode().get(getInNode().size() - 2).equals("location")) {
            if (getInNode().lastElement().equals("locationName"))
                city = fetchStr;
        }

        if (getInNode().size() >= 6
                && getInNode().get(getInNode().size() - 6).equals("cwbopendata")
                && getInNode().get(getInNode().size() - 5).equals("dataset")
                && getInNode().get(getInNode().size() - 4).equals("location")
                && getInNode().get(getInNode().size() - 3).equals("weatherElement")
                && getInNode().get(getInNode().size() - 2).equals("time")) {
            if (getInNode().lastElement().equals("startTime"))
                date = fetchStr;
        }

        if (getInNode().size() >= 7
                && getInNode().get(getInNode().size() - 7).equals("cwbopendata")
                && getInNode().get(getInNode().size() - 6).equals("dataset")
                && getInNode().get(getInNode().size() - 5).equals("location")
                && getInNode().get(getInNode().size() - 4).equals("weatherElement")
                && getInNode().get(getInNode().size() - 3).equals("time")
                && getInNode().get(getInNode().size() - 2).equals("parameter")) {
            if (getInNode().lastElement().equals("parameterName")) {
                wdItem.setStrCity(city);
                wdItem.setStrDateTime(date);
                wdItem.setStrWeather(fetchStr);
            }
            if (getInNode().lastElement().equals("parameterValue")) {
                wdItem.setParameterValue(fetchStr);
            }

        }

    }

}
