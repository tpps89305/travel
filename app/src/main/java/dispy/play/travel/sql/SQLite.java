package dispy.play.travel.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 建立本機資料庫，以儲存資料。
 * Created by yangchaofu on 2015/7/12.
 */
public class SQLite extends SQLiteOpenHelper {

    private static final int VERSION = 1;//資料庫版本
    // 資料庫物件，固定的欄位變數
    private static SQLiteDatabase database;
    private static final String DATABASE_NAME = "DispyDB"; //資料庫名稱

    public SQLite(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public SQLite(Context context,String name) {
        this(context, name, null, VERSION);
    }

    public SQLite(Context context, String name, int version) {
        this(context, name, null, version);
    }

    protected static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new SQLite(context, DATABASE_NAME,
                    null, VERSION).getWritableDatabase();
        }
        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String DATABASE_CREATE_TABLE =
                "create table ScheduleList("
                        + "m_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                        + "se_no INTEGER,"
                        + "name VARCHAR,"
                        + "image VARCHAR"
                        + ")";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE =
                "create table ScheduleDetail("
                + "no INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "d_id INTEGER NOT NULL,"
                + "m_id INTEGER,"
                + "name VARCHAR,"
                + "latitude REAL,"
                + "longitude REAL," +
                        "content VARCHAR"
                + ")";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE =
                "create table Weather("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "city VARCHAR,"
                + "time VARCHAR,"
                + "weather VARCHAR,"
                        + "temp VARCHAR,"
                + "value VARCHAR)";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE =
                "create table Culture(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        "title VARCHAR," +
                        "category VARCHAR," +
                        "address VARCHAR," +
                        "place VARCHAR," +
                        "latitude VARCHAR," +
                        "longitude VARCHAR," +
                        "description VARCHAR," +
                        "start VARCHAR," +
                        "end VARCHAR," +
                        "show_unit VARCHAR," +
                        "master_unit VARCHAR," +
                        "price VARCHAR," +
                        "link VARCHAR)";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE =
                "create table Food(" +
                        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                        "title VARCHAR," +
                        "address VARCHAR," +
                        "phone VARCHAR," +
                        "route VARCHAR," +
                        "description VARCHAR," +
                        "link VARCHAR," +
                        "latitude VARCHAR," +
                        "longitude VARCHAR," +
                        "modify_date VARCHAR)";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE = "create table Article(" +
                "id VARCHAR)";
        db.execSQL(DATABASE_CREATE_TABLE);
        DATABASE_CREATE_TABLE = "create table Comment(" +
                "id VARCHAR)";
        db.execSQL(DATABASE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS newMemorandum"); //刪除舊有的資料表
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        //每次成功打開數據庫後首先被執行
    }

    @Override
    public synchronized void close() {
        super.close();
    }
}
