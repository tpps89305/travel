package dispy.play.travel.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.object.Weather;

/** 本機資料庫存取功能
 * @author dispyTranslate
 * @since 2015/7/12
 * **/
public class SQLiteHandler {

    private final String TAG = this.getClass().getSimpleName();
    private SQLiteDatabase mDB;

    public SQLiteHandler(Context context) {
        mDB = SQLite.getDatabase(context);
    }

    /** 暫存使用者的文章列表
     * @param jsonArray 儲存文章 ID 的陣列
     * **/
    public void saveUserArticleId (String jsonArray) {
        try {
            JSONArray array = new JSONArray(jsonArray);
            ContentValues cv = new ContentValues();
            for (int i = 0; i < array.length(); i++) {
                cv.put("id", array.getString(i));
                mDB.insert("Article", null, cv);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "無法將此字串解析成 JSONArray", e);
        }
    }

    /** 暫存使用者的留言列表
     * @param jsonArray 儲存留言 ID 的陣列
     * **/
    public void saveUserComment (String jsonArray) {
        try {
            JSONArray array = new JSONArray(jsonArray);
            ContentValues cv = new ContentValues();
            for (int i = 0; i < array.length(); i++) {
                cv.put("id", array.getString(i));
                mDB.insert("Comment", null, cv);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "無法將此字串解析成 JSONArray", e);
        }
    }

    public String[] getCommentIDArray() {
        Cursor cursor = mDB.rawQuery("select id from Comment", null);
        int rowsCount = cursor.getCount();
        String[] result = new String[rowsCount];
        cursor.moveToFirst();
        for (int i = 0; i < rowsCount; i++) {
            result[i] = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    public static String[][] getScheduleList(SQLiteDatabase db, String table) {
        Cursor cursor = db.rawQuery(
                "select m_id, name from " + table, null
        );
        int rowsCount = cursor.getCount();
        String[][] result = new String[rowsCount][2];
        String temp;
        cursor.moveToFirst();
        for (int i = 0; i < rowsCount; i++) {
            temp = cursor.getString(0);
            result[i][0] = temp;
            temp = cursor.getString(1);
            result[i][1] = temp;
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    public static long saveToScheduleDetail
            (SQLiteDatabase db, int m_id, int d_id, String name, LatLng latLng, String content) {

        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("latitude", latLng.latitude);
        cv.put("longitude", latLng.longitude);
        cv.put("m_id", m_id);
        cv.put("d_id", d_id);
        cv.put("content", content);
        long result = db.insert("ScheduleDetail", null, cv);
        return result;
    }

    public static String getSeNoFromDB(SQLiteDatabase db, int m_id) {
        Cursor cursor = db.rawQuery("select se_no from ScheduleList where m_id = " + m_id, null);
        cursor.moveToFirst();
        String result = cursor.getString(0);
        cursor.close();
        return result;
    }

    public static void deleteAllData(SQLiteDatabase db) {
        db.delete("ScheduleDetail", null, null);
        db.delete("ScheduleList", null, null);
    }

    public static List<HashMap<String, String>> getScheduleDetail(SQLiteDatabase db, String m_id) {
        Cursor cursor = db.rawQuery(
                "select d_id, name, latitude, longitude from " + "ScheduleDetail" + " WHERE m_id = " + m_id, null
        );
        int rowsCount = cursor.getCount();
        List<HashMap<String, String>> result = new ArrayList<HashMap<String,String>>();
        HashMap<String, String> temp = new HashMap<String, String>();
        if (rowsCount == 0) {
            return null;
        } else {
            cursor.moveToFirst();

            for (int i = 0; i < rowsCount; i++) {
                temp = new HashMap<>();
                temp.put("d_id", cursor.getString(0));
                temp.put("name", cursor.getString(1));
                temp.put("latitude", cursor.getString(2));
                temp.put("longitude", cursor.getString(3));
                result.add(temp);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return result;
    }

    public static void deleteScheduleListItem(SQLiteDatabase db, String m_id) {
        String where = "m_id = " + m_id;
        db.delete("ScheduleDetail", where, null);
        db.delete("ScheduleList", where, null);
    }

    public static void addToScheduleList(SQLiteDatabase db, String name, String image) {

        ContentValues cv;
        cv = new ContentValues();
        cv.put("name", name);
        cv.put("image", image);
        db.insert("ScheduleList", null, cv);
    }

    public static void addSeNo (SQLiteDatabase db, String m_id, String se_no) {
        ContentValues cv = new ContentValues();
        cv.put("se_no", se_no);
        String where = "m_id" + "=" + m_id;
        db.update("ScheduleList", cv, where, null);
    }

    public static String getPicName (SQLiteDatabase db, String m_id) {
        Cursor cursor = db.rawQuery(
                "select image from " + "ScheduleList" + " WHERE m_id = " + m_id, null
        );
        int rowsCount = cursor.getCount();
        String result;
        if (rowsCount == 0) {
            return null;
        } else {
            cursor.moveToFirst();
            result = cursor.getString(0);
            cursor.close();
        }
        return result;
    }

    public static void getCultureData(SQLiteDatabase db, JSONArray[] jsonArrays)
            throws IOException, JSONException {

        JSONObject jsonObject;
        JSONArray jsonArray;
        ContentValues cv;
        JSONArray temp;

        for(int i = 0; i < jsonArrays.length; i++) {
            jsonArray = jsonArrays[i];

            for (int j = 0; j < jsonArray.length(); j++) {
                jsonObject = jsonArray.getJSONObject(j);

                cv = new ContentValues();
                cv.put("title", jsonObject.getString("title"));
                cv.put("category", jsonObject.getString("category"));
                cv.put("show_unit", jsonObject.getString("showUnit"));
                cv.put("description", jsonObject.getString("descriptionFilterHtml"));
                cv.put("link", jsonObject.getString("sourceWebPromote"));
                cv.put("master_unit", jsonObject.getString("masterUnit"));

                temp = jsonObject.getJSONArray("showInfo");
                try {
                    jsonObject = temp.getJSONObject(0);
                    if (!jsonObject.getString("latitude").equals("")
                            && !jsonObject.getString("latitude").equals("0")) {
                        cv.put("start", jsonObject.getString("time"));
                        cv.put("place", jsonObject.getString("locationName"));
                        cv.put("price", jsonObject.getString("price"));
                        cv.put("address", jsonObject.getString("location"));
                        cv.put("latitude", jsonObject.getString("latitude"));
                        cv.put("longitude", jsonObject.getString("longitude"));
                        cv.put("end", jsonObject.getString("endTime"));
                        db.insert("Culture", null, cv);

                    }

                } catch (JSONException e) {
                    continue;
                }
            }

        }

    }

    public static void getWeatherData (SQLiteDatabase db, Weather[] weatherData, String city) {
        ContentValues cv;

        Weather[] input = new Weather[45];
        int j = 0;
        for(int i = 0; i < weatherData.length; i++) {
            if(weatherData[i].getStrCity().equals(city)){
                input[j] = weatherData[i];
                j++;
                if(j >= 45)
                    break;
            }
        }

        for(int i = 0; i < j / 3; i++) {

            String maxT = input[i + (j / 3)].getStrWeather();
            String minT = input[i + (j / 3) * 2].getStrWeather();

            cv = new ContentValues();
            cv.put("city", input[i].getStrCity());
            cv.put("time", input[i].getStrDateTime());
            cv.put("weather", input[i].getStrWeather());
            cv.put("temp", maxT + "C~" + minT + "C");
            cv.put("value", input[i].getParameterValue());
            db.insert("Weather", null, cv);
        }
    }

    public static String[][] getWeatherDataFromDB (SQLiteDatabase db, String city) {

        Cursor cursor = db.rawQuery(
                "select time, weather, temp, value from " + "Weather" + " WHERE city = '" + city + "'", null
        );
        String[][] result;
        int rowsCount = cursor.getCount();
        if (rowsCount == 0) {
            return null;
        } else {
            result = new String[rowsCount][4];
            cursor.moveToFirst();
            for(int i = 0; i < rowsCount; i++) {
                for(int j = 0; j < 4; j++) {
                    result[i][j] = cursor.getString(j);
                }
                cursor.moveToNext();
            }
            cursor.close();
        }
        return result;
    }

    public static String[][] getCultureMarkInfo(SQLiteDatabase db, String city, int tag) {

        String select =
                "select title, address, latitude, longitude, id from Culture where address LIKE %" + city + "%";
        String where = " OR ";
        switch (tag) {
            case 0:
                where += "category=1 OR category=5 OR category=17";
                break;
            case 1:
                where += "category=2 OR category=3 OR category=6 OR category=8";
                break;
            case 2:
                where += "category=4";
                break;
            case 3:
                where += "category=13 OR category=14";
                break;
        }
        Cursor cursor = db.rawQuery(select + where, null);
        String[][] result;
        int rowsCount = cursor.getCount();
        if (rowsCount == 0) {
            return null;
        } else {
            result = new String[rowsCount][5];
            cursor.moveToFirst();
            for(int i = 0; i < rowsCount; i++) {
                for(int j = 0; j < 5; j++) {
                    result[i][j] = cursor.getString(j);
                }
            }
            cursor.close();
        }

        return result;
    }

    public static String[] getCultureDetail (SQLiteDatabase db, String id) {
        String select = "select title, address, place, description, start, end from Culture where id=" + id;

        Cursor cursor = db.rawQuery(select, null);
        String[] result = new String[6];
        for(int i = 0; i < 6; i++)
            result[i] = cursor.getString(i);
        cursor.close();
        return result;

    }

    public static void deleteWeather (SQLiteDatabase db) {
        db.delete("Weather", null, null);
    }

    public static void deleteCulture (SQLiteDatabase db) {
        db.delete("Culture", null, null);
    }

    public static void getFoodDataFromWeb(SQLiteDatabase db, JSONArray jsonArray)
            throws IOException, JSONException {

        JSONObject jsonObject;
        ContentValues cv = new ContentValues();
        for(int i = 0; i < jsonArray.length(); i++){
            jsonObject = jsonArray.getJSONObject(i);

            cv.put("title", jsonObject.getString("title"));
            cv.put("address", jsonObject.getString("address"));
            cv.put("phone", jsonObject.getString("phone"));
            cv.put("route", jsonObject.getString("route"));
            cv.put("description", jsonObject.getString("Description"));
            cv.put("link", jsonObject.getString("link"));
            cv.put("latitude", jsonObject.getString("y"));
            cv.put("longitude", jsonObject.getString("x"));
            cv.put("modify_date", jsonObject.getString("modify_date"));
            db.insert("Food", null, cv);
        }
    }

    public static String[][] getFoodInfo (SQLiteDatabase db, String city) {
        String select = "select title, address, latitude, longitude from Food where address LIKE '%" + city +"%'";

        Cursor cursor = db.rawQuery(select, null);
        int rowsCount = cursor.getCount();
        String[][] result;
        if (rowsCount == 0) {
            return null;
        } else {
            result = new String[rowsCount][4];
            cursor.moveToFirst();
            for(int i = 0; i < rowsCount; i++) {
                for(int j = 0; j < 4; j++) {
                    result[i][j] = cursor.getString(j);
                }
                cursor.moveToNext();
            }
            cursor.close();
        }
        return result;

    }

    public static void deleteFood (SQLiteDatabase db) {
        db.delete("Food", null, null);
    }

    public static String[] getFoodDetail (SQLiteDatabase db, String title) {
        String select =
                "select title, address, route, phone, description, link, modify_date from Food where title ='" + title + "'";
        Cursor cursor = db.rawQuery(select, null);
        String[] result = new String[7];
        for(int i = 0; i < 7; i++)
            result[i] = cursor.getString(i);
        cursor.close();
        return result;
    }

    public static String getContent (SQLiteDatabase db, String d_id) {
        String select = "SELECT content FROM ScheduleDetail WHERE d_id = " + d_id;
        Cursor cursor = db.rawQuery(select, null);
        String result = cursor.getString(0);
        cursor.close();
        return result;
    }

    public static String[][] getRouteMarker(SQLiteDatabase db, String m_id) {
        Cursor cursor = db.rawQuery(
                "select name, latitude, longitude from ScheduleDetail WHERE m_id = " + m_id, null
        );
        int rowsCount = cursor.getCount();
        if(rowsCount <= 0) {
            return null;
        }
        else {
            String[][] result = new String[rowsCount][3];
            String temp;
            cursor.moveToFirst();
            for(int i = 0; i < rowsCount; i++){
                temp = cursor.getString(0);
                result[i][0] = temp;
                temp = cursor.getString(1);
                result[i][1] = temp;
                temp = cursor.getString(2);
                result[i][2] = temp;
                cursor.moveToNext();
            }
            cursor.close();
            return result;
        }

    }

}
