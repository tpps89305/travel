package dispy.play.travel.net;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.ParserConfigurationException;

import dispy.play.travel.WeatherDataReader;
import dispy.play.travel.object.Weather;
import dispy.play.travel.XMLParser;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * 網路功能。
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017-08-24
 */

public class NetworkHandler {

    private static final String TAG = "NetworkHandler";

    /**
     * 取得美食類開放資料。依照使用者所在地，提供在當地的資訊。
     * 資料來源：行政院農業委會 - 農業美食。
     *
     * @param adminArea 使用者所在的縣市級行政區（例：高雄市、台北市）
     * @param listener  搭配使用偵聽器，以接收回傳結果。
     **/
    public static void getCoaFoodData(final String adminArea, final onReceivedOpenDataListener listener) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://data.coa.gov.tw/Service/OpenData/CC/").build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<ResponseBody> call = openDataAPI.getCoaFoodData();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                List<HashMap<String, String>> result = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject;
                    HashMap<String, String> temp;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        temp = new HashMap<>();
                        temp.put("title", jsonObject.getString("title"));
                        temp.put("address", jsonObject.getString("address"));
                        temp.put("business_hrs", jsonObject.getString("business_hrs"));
                        temp.put("description", jsonObject.getString("Description"));
                        temp.put("phone", jsonObject.getString("phone"));
                        temp.put("price", jsonObject.getString("price"));
                        temp.put("route", jsonObject.getString("route"));
                        temp.put("latitude", jsonObject.getString("y"));
                        temp.put("longitude", jsonObject.getString("x"));
                        temp.put("modify_date", jsonObject.getString("modify_date"));
                        if (temp.get("address").contains(adminArea))
                            result.add(temp);
                    }
                    listener.done(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 Coa JSON 時發生錯誤", e);
                    listener.done(null);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 Coa JSON 時發生錯誤", e);
                    listener.done(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "下載 Coa 資料時發生錯誤", t);
                listener.done(null);
            }
        });
    }

    /**
     * 取得導航資訊。
     *
     * @param originPosition      起點，即使用者的所在位置。
     * @param destinationPosition 終點，即該資訊標註的位置。
     * @param listener            搭配使用偵聽器，以接收回傳結果。
     **/
    public static void getDirectionsJSON(LatLng originPosition, LatLng destinationPosition, final onReceivedMapDataListener listener) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/directions/").build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        String origin = String.valueOf(originPosition.latitude) + "," + String.valueOf(originPosition.longitude);
        String destination = String.valueOf(destinationPosition.latitude) + "," + String.valueOf(destinationPosition.longitude);
        Call<ResponseBody> call = openDataAPI.getDirections(origin, destination, "zh-TW");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    listener.done(new JSONObject(response.body().string()));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 Directions JSON 時發生錯誤", e);
                    listener.done(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "下載導航資料時發生錯誤", t);
                listener.done(null);
            }
        });
    }

    /**
     * 取得登記的聚落資訊
     * 資料來源：文化部文化資產局 - 聚落資訊
     *
     * @param adminArea 要檢索的地區
     * @param listener  搭配使用偵聽器，以接收回傳結果。
     **/
    public static void getBOCHSettlementData(final String adminArea, final onReceivedOpenDataListener listener) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://cloud.culture.tw/frontsite/trans/").client(getUnsafeOkHttpClient()).build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<ResponseBody> call = openDataAPI.getCultureData("exportEmapJson", "A", "1.3");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject;
                    List<HashMap<String, String>> result = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put("title", jsonObject.getString("name"));
                        temp.put("level", jsonObject.getString("level"));
                        temp.put("address", jsonObject.getString("address"));
                        temp.put("longitude", jsonObject.getString("longitude"));
                        temp.put("latitude", jsonObject.getString("latitude"));
                        temp.put("registerDateValue", jsonObject.getString("registerDateValue"));
                        temp.put("srcWebsite", jsonObject.getString("srcWebsite"));

                        String[] cityName = jsonObject.getString("cityName").split(" ");
                        temp.put("adminArea", cityName[0]);
                        temp.put("locality", cityName[1]);
                        if (cityName[0].equals(adminArea))
                            result.add(temp);
                    }
                    listener.done(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 BOCHSettlement JSON 時發生錯誤", e);
                    listener.done(null);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 BOCHSettlement JSON 時發生錯誤", e);
                    listener.done(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "下載 BOCHSettlement 資料時發生錯誤", t);
                listener.done(null);
            }
        });

    }

    public static void getBOCHHistoricalSitesData(final String adminArea, final onReceivedOpenDataListener listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://cloud.culture.tw/frontsite/trans/")
                .client(getUnsafeOkHttpClient()).build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<ResponseBody> call = openDataAPI.getCultureData("exportEmapJson", "A", "1.1");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject;
                    List<HashMap<String, String>> result = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put("title", jsonObject.getString("name"));
                        temp.put("level", jsonObject.getString("level"));
                        temp.put("address", jsonObject.getString("address"));
                        temp.put("longitude", jsonObject.getString("longitude"));
                        temp.put("latitude", jsonObject.getString("latitude"));
                        temp.put("buildingYear", jsonObject.getString("buildingYearName") + " (" + jsonObject.getString("buildingCreateWestYear") + ")");
                        temp.put("registerDateValue", jsonObject.getString("registerDateValue"));
                        temp.put("srcWebsite", jsonObject.getString("srcWebsite"));

                        String[] cityName = jsonObject.getString("cityName").split(" ");
                        temp.put("adminArea", cityName[0]);
                        if (cityName[0].equals(adminArea))
                            result.add(temp);
                    }
                    listener.done(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 BOCHHistoricalSites JSON 時發生錯誤", e);
                    listener.done(null);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "解析 BOCHHistoricalSites JSON 時發生錯誤", e);
                    listener.done(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "下載 BOCHHistoricalSites 資料時發生錯誤", t);
                listener.done(null);
            }
        });
    }

    /**
     * 使用 Https 協定時要用的功能。
     *
     * @return OkHttpClient
     **/
    private static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


    /**
     * 取得天氣圖示
     * @param paramValue 圖示的代號（個位數要補 0）
     * @return 存成 Bitmap 變數的天氣圖示
     * @throws IOException
     */
    public static Bitmap getWeatherIcon (String paramValue) throws IOException {
        String picUrl = "http://www.cwb.gov.tw/V7/symbol/weather/gif/day/";
        URL url = new URL(picUrl + paramValue + ".gif");
        return BitmapFactory.decodeStream(url.openConnection().getInputStream());
    }

    /**
     * 取得天氣 OpenData
     * @param city2 城市名（正字）
     * @return 天氣 OpenData
     */
    public static List<HashMap<String, String>> getWeatherData(String city2){
        String url = "http://opendata.cwb.gov.tw/opendata/MFC/F-C0032-005.xml";
        Weather[] temp;
        HashMap<String ,String> tempMap;
        List<HashMap<String, String>> tempList = new ArrayList<>();
        List<HashMap<String, String>> output = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SSSZ");
        Date date;
        try{
            XMLParser xp = new XMLParser(new WeatherDataReader());
            Object[] data = xp.getData(url);
            if(data != null)
                if(data[0] instanceof Weather[]) {
                    temp = (Weather[]) data[0];
                    for(int i = 0; i < temp.length; i++) {
                        if(temp[i].getStrCity().equals(city2)) {
                            tempMap = new HashMap<>();
                            tempMap.put("city", temp[i].getStrCity());
                            tempMap.put("date",temp[i].getStrDateTime());
                            tempMap.put("weather", temp[i].getStrWeather());
                            tempMap.put("value", temp[i].getParameterValue());
                            tempList.add(tempMap);
                        }
                    }
                    for(int i = 0; i < tempList.size() / 3; i++) {
                        tempMap = new HashMap<>();
                        date = sdf.parse(tempList.get(i).get("date"));
                        String icon = tempList.get(i).get("value");
                        if (icon.length() <= 1) {
                            StringBuffer buf = new StringBuffer();
                            buf.append(icon);
                            buf.insert(0, "0");
                            icon = buf.toString();
                        }

                        tempMap.put("city", tempList.get(i).get("city"));
                        tempMap.put("date", String.valueOf(date.getMonth() + 1) + " 月 " + String.valueOf(date.getDate()) + " 日 - " + String.valueOf(date.getHours() + " 時"));
                        tempMap.put("icon", icon);
                        tempMap.put("weather", tempList.get(i).get("weather"));
                        tempMap.put("temperature", tempList.get(i + (tempList.size() * 2 / 3)).get("weather") + "~" + tempList.get(i + tempList.size() / 3).get("weather") + "°C");
                        output.add(tempMap);
                    }
                    return output;
                }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface onReceivedOpenDataListener {
        void done(List<HashMap<String, String>> result);
    }

    public interface onReceivedMapDataListener {
        void done(JSONObject result);
    }

    public interface DataReceiveListener {
        void done(String result);
    }

}
