package dispy.play.travel.net;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dispy.play.travel.object.HistoricalSitesData;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by yangchaofu on 2017/12/5.
 *
 * @author yangchaofu
 * @since 2017/12/5
 */

public class GetHistoricalSitesData {

    public static void get(final String adminArea, final Listener listener) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://cloud.culture.tw/frontsite/trans/")
                .client(getUnsafeOkHttpClient()).build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<ResponseBody> call = openDataAPI.getCultureData("exportEmapJson", "A", "1.1");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    List<HistoricalSitesData> listData = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        HistoricalSitesData data = new HistoricalSitesData(jsonObject);
                        if (data.getAdminArea().contains(adminArea)) {
                            listData.add(data);
                        }
                    }
                    listener.done(listData);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public static Marker[] putToMap(GoogleMap map, List<HistoricalSitesData> data) {
        Marker[] result = new Marker[data.size()];
        MarkerOptions markerOptions;
        for (int i = 0; i < data.size(); i++) {
            markerOptions = new MarkerOptions();
            markerOptions.title(data.get(i).getName());
            markerOptions.snippet(data.get(i).getAddress());
            markerOptions.position(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude()));
            result[i] = map.addMarker(markerOptions);
            result[i].setTag(i);
        }
        return result;
    }

    /**
     * 使用 Https 協定時要用的功能。
     *
     * @return OkHttpClient
     **/
    private static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public interface Listener {
        void done(List<HistoricalSitesData> result);
    }
}
