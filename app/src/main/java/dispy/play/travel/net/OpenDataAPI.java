package dispy.play.travel.net;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 開放資料下載界面。此界面搭配 Retrofit 使用。
 *
 * @author dispyTranslate
 * @since 2017-08-25
 * @version 1.0
 */

interface OpenDataAPI {

    //行政院農業美食
    @GET("FoodData.aspx")
    Call<ResponseBody> getCoaFoodData();

    //Google 導航
    @GET("json")
    Call<ResponseBody> getDirections(@Query("origin") String origin, @Query("destination") String destination, @Query("language") String language);

    //文資局
    //classifyid 編號對照：
    //1.3 聚落資訊
    //1.1 古蹟資訊
    @GET("emapOpenDataAction.do")
    Call<ResponseBody> getCultureData(@Query("method") String method, @Query("typeId") String typeId, @Query("classifyId") String classifyId);

}
