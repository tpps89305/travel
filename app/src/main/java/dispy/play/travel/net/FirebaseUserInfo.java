package dispy.play.travel.net;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.net.URL;
import java.util.List;

import retrofit2.http.Url;

/**
 * Created by yangchaofu on 2017/12/11.
 *
 * @author yangchaofu
 * @since 2017/12/11
 */

public class FirebaseUserInfo {

    private final String TAG = this.getClass().getSimpleName();

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private static FirebaseUserInfo instance;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private UserInfoUpdateListener listener;

    private FirebaseUserInfo() {
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
    }

    public static FirebaseUserInfo getInstance() {
        if (instance == null) {
            instance = new FirebaseUserInfo();
        }
        return instance;
    }

    public void setListener(UserInfoUpdateListener listener) {
        this.listener = listener;
    }

    /**
     * 初始化 Firebase 帳戶功能
     **/
    public void initFireBaseAth() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    writeUserDataToDatabase();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                getLoginInfo();
            }
        };
    }

    public void getLoginInfo() {
        if (user != null) {
            listener.onGetInfo(user.getDisplayName(), user.getEmail(), user.getPhotoUrl());
        } else {
            listener.onNoUser();
        }
    }

    private void writeUserDataToDatabase() {
        String reference = "user/" + user.getUid() + "/";
        List<? extends UserInfo> profile = user.getProviderData();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(reference + "displayName");
        myRef.setValue(profile.get(0).getDisplayName());
        myRef = database.getReference(reference + "email");
        myRef.setValue(profile.get(0).getEmail());
        myRef = database.getReference(reference + "providerId");
        myRef.setValue(profile.get(1).getProviderId());
        myRef = database.getReference(reference + "photoUrl");
        if (profile.get(1).getProviderId().equals("facebook.com"))
            myRef.setValue(profile.get(0).getPhotoUrl().toString());
        else if (profile.get(1).getProviderId().equals("password")) {
            try {
                myRef.setValue("https://" + profile.get(0).getPhotoUrl().getAuthority() + profile.get(0).getPhotoUrl().getEncodedPath());
            } catch (NullPointerException e) {
                Log.e(TAG, "將頭相寫入到資料庫時發生錯誤", e);
            }

        }
    }

    public void addAuthStateListener() {
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void removeAuthStateListener() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public boolean hasUser() {
        return user != null;
    }

    public interface UserInfoUpdateListener {
        void onGetInfo(String displayName, String email, Uri photoUrl);
        void onNoUser();
    }

}
