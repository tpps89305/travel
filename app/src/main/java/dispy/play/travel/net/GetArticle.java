package dispy.play.travel.net;

import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.object.Article;

/**
 * Created by yangchaofu on 2017/12/8.
 *
 * @author yangchaofu
 * @since 2017/12/8
 */

public class GetArticle {

    public static void get(String adminArea, final Listener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("article");
        Query query = myRef.orderByChild("adminArea").equalTo(adminArea);
        // Read from the database
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Article> result = new ArrayList<>();
                for (DataSnapshot subDataSnapshot : dataSnapshot.getChildren()) {
                    Article article = new Article((HashMap<String, Object>) subDataSnapshot.getValue());
                    article.setPostId(subDataSnapshot.getKey());
                    result.add(article);
                }
                listener.done(result);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(getClass().getSimpleName(), "Failed to read value.", error.toException());
                listener.onFailure();
            }
        });
    }

    public static Marker[] putToMap(GoogleMap map, List<Article> data) {
        Marker[] result = new Marker[data.size()];
        MarkerOptions markerOptions;
        for (int i = 0; i < data.size(); i++) {
            markerOptions = new MarkerOptions();
            markerOptions.title(data.get(i).getContent());
            markerOptions.position(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude()));
            result[i] = map.addMarker(markerOptions);
            result[i].setTag(data.get(i).getPostId());
        }
        return result;
    }

    public interface Listener {
        void done(List<Article> result);

        void onFailure();
    }
}
