package dispy.play.travel.net;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.R;
import dispy.play.travel.StringHandler;

/**
 * Firebase 相關功能。
 *
 * @author dispyTranslate
 * @version 1.0
 * @since 2017/8/31
 */

public class FirebaseHandler {

    private final String TAG = this.getClass().getSimpleName();

    private FirebaseAuth mAuth;
    private static FirebaseUser user;
    private static FirebaseHandler instance;

    private String mProviderId;

    private FirebaseHandler() {
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
    }

    public static FirebaseHandler getInstance() {
        if (instance == null) {
            instance = new FirebaseHandler();
        }
        return instance;
    }

    /**
     * 上傳使用者頭相
     *
     * @param file     通用資源識別子（Uri）檔案
     * @param listener 處理回傳結果的偵聽器
     **/
    public void updateProfilePhoto(Uri file, final UpdateProfilePhotoListener listener) {
        user = mAuth.getCurrentUser();
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = storageRef.child("user/" + user.getUid() + "/profile/photo.jpg");
        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        @SuppressWarnings("VisibleForTests")
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        updateUserInfo(downloadUrl, listener);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        listener.onFailure(exception);
                        Log.e(TAG, "上傳相片時發生錯誤", exception);
                    }
                });
    }

    /**
     * 更新使用者的䁥稱
     *
     * @param displayName 新的䁥稱.
     * @param listener    處理回傳結果的偵聽器
     **/
    public void updateDisplayName(final String displayName, final UpdateDataListener listener) {
        user = mAuth.getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(displayName)
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                            updateCommentDisplayName(displayName, listener);
                        }
                    }
                });
    }

    /**
     * 更新使用者頭相。搭配 <code>updateProfilePhoto</code> 使用。
     *
     * @param photoUri 在 <code>updateProfilePhoto</code> 中得到的 Uri.
     * @param listener 處理回傳結果的偵聽器
     **/
    private void updateUserInfo(Uri photoUri, final UpdateProfilePhotoListener listener) {
        user = mAuth.getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(photoUri) //將新的使用者頭相寫入使用者資訊
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            listener.onSuccess();
                            Log.d(TAG, "使用者照片更新成功");
                        }
                    }
                });
    }

    public void updateUserIntroduce(String introduce) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String reference = "user/" + getUid() + "/";
        DatabaseReference myRef = database.getReference(reference + "introduce");
        myRef.setValue(introduce);
    }

    public String getUid() {
        user = mAuth.getCurrentUser();
        return user.getUid();
    }

    public Uri getPhotoUrl() {
        user = mAuth.getCurrentUser();
        return user.getPhotoUrl();
    }

    public String getDisplayName() {
        user = mAuth.getCurrentUser();
        return user.getDisplayName();
    }

    public void signOut() {
        user = mAuth.getCurrentUser();
        if (mProviderId != null & mProviderId.equals("facebook.com"))
            LoginManager.getInstance().logOut();
        mAuth.signOut();
    }

    public String getEmail() {
        user = mAuth.getCurrentUser();
        return user.getEmail();
    }

    /**
     * 取得自己的使用者資訊
     *
     * @param listener 處理回傳結果的偵聽器
     **/
    public void getUserInfo(final GetUserInfoListener listener) {
        user = mAuth.getCurrentUser();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String reference = "user/" + user.getUid();
        DatabaseReference myRef = database.getReference(reference);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, String> value = (HashMap<String, String>) dataSnapshot.getValue();
                String birthday = value.get("birthday");
                String introduce = value.get("introduce");
                if (birthday == null)
                    birthday = "沒有提供";
                if (introduce == null)
                    introduce = "";
                Log.d(TAG, "birthday: " + birthday);
                Log.d(TAG, "introduce: " + introduce);

                List<? extends UserInfo> profile = user.getProviderData();
                List<HashMap<String, String>> userData = new ArrayList<>();
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("title", "名字");
                hashMap.put("content", profile.get(0).getDisplayName());
                userData.add(hashMap);
                hashMap = new HashMap<>();
                hashMap.put("title", "帳號");
                hashMap.put("content", profile.get(0).getEmail());
                userData.add(hashMap);
                hashMap = new HashMap<>();
                hashMap.put("title", "登入方式");
                hashMap.put("content", profile.get(1).getProviderId());
                userData.add(hashMap);

                mProviderId = profile.get(1).getProviderId();

                hashMap = new HashMap<>();
                hashMap.put("title", "UID");
                hashMap.put("content", profile.get(0).getUid());
                userData.add(hashMap);
                hashMap = new HashMap<>();
                hashMap.put("title", "生日");
                hashMap.put("content", birthday);
                userData.add(hashMap);
                hashMap = new HashMap<>();
                hashMap.put("title", "自我介紹");
                hashMap.put("content", introduce);
                userData.add(hashMap);
                listener.done(userData, birthday, introduce);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "接收使用者資訊時發生錯誤", databaseError.toException());
            }
        });

    }

    /**
     * 取得文章的作者資訊：䁥稱、頭相 Url。
     *
     * @param authorUid 作者的 UID
     * @param listener 專用偵聽器
     * **/
    public void getAuthorInfo(String authorUid, final GetAuthorInfoListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String reference = "user/" + authorUid;
        DatabaseReference myRef = database.getReference(reference);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, String> userInfo = (HashMap<String, String>) dataSnapshot.getValue();
                listener.done(userInfo.get("displayName"), userInfo.get("photoUrl"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "取得作者資訊時發生錯誤", databaseError.toException());
            }
        });
    }


    /**
     * 取得文章的相片
     *
     * @param photoUrl 相片名稱
     * @param toShow   用來顯示圖片的 ImageView
     **/
    public void getArticlePhoto(String photoUrl, final ImageView toShow) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference("article/" + photoUrl);
        try {
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            toShow.setImageURI(Uri.fromFile(localFile));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "文章圖片載入失敗", exception);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 取得文章作者的頭相
     *
     * @param authorUid 作者的 UID
     * @param toShow   用來顯示圖片的 ImageView
     **/
    public void getAuthorPhoto(String authorUid, final ImageView toShow) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference("user/" + authorUid + "/profile/photo.jpg");
        try {
            final File localFile = File.createTempFile("photo", "jpg");
            storageRef.getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            toShow.setImageURI(Uri.fromFile(localFile));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG, "作者頭相載入失敗", exception);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新每則留言內的名字。
     * 因應 Firebase 資料庫是 noSQL，以反正規化的原則與資料庫互動。
     *
     * @param displayName 新的䁥稱
     * @param listener    處理回傳結果的偵聽器
     **/
    private void updateCommentDisplayName(final String displayName, final UpdateDataListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("comment");
        Query query = myRef.orderByChild("uid").equalTo(getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, HashMap<String, String>> subDataSnapshot = (HashMap<String, HashMap<String, String>>) dataSnapshot.getValue();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myCommentRef;

                for (String commentId : subDataSnapshot.keySet()) {
                    myCommentRef = database.getReference("comment/" + commentId + "/displayName");
                    myCommentRef.setValue(displayName);
                    Log.i(TAG, "已變更 " + commentId + " 中的 displayName 為 " + displayName);
                }
                listener.onSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "更新䁥稱時發生錯誤", databaseError.toException());
            }
        });

    }

    /**
     * 依據地區列出使用者的貼文。
     *
     * @param adminArea 縣市級行政區。例：台北市、高雄市、桃園縣。
     * @param locality  鄉鎮市級行政區。例：松山區、小港區、桃園市。
     * @param listener  處理回傳結果的偵聽器。
     **/
    public void getArticle(String adminArea, String locality, final ArticleListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("article");
        Query query = myRef.orderByChild("adminArea_locality").equalTo(adminArea + locality);
        // Read from the database
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<HashMap<String, Object>> result = new ArrayList<>();
                for (DataSnapshot subDataSnapshot : dataSnapshot.getChildren()) {
                    HashMap<String, Object> value = (HashMap<String, Object>) subDataSnapshot.getValue();
                    value.put("postId", subDataSnapshot.getKey());
                    result.add(value);
                }
                listener.done(result);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                listener.onFailure();
            }
        });

    }

    /**
     * 依據地區列出使用者的貼文。
     *
     * @param adminArea 縣市級行政區。例：台北市、高雄市、桃園縣。
     * @param listener  處理回傳結果的偵聽器。
     **/
    public void getArticle(String adminArea, final ArticleListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("article");
        Query query = myRef.orderByChild("adminArea").equalTo(adminArea);
        // Read from the database
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<HashMap<String, Object>> result = new ArrayList<>();
                for (DataSnapshot subDataSnapshot : dataSnapshot.getChildren()) {
                    HashMap<String, Object> value = (HashMap<String, Object>) subDataSnapshot.getValue();
                    value.put("postId", subDataSnapshot.getKey());
                    result.add(value);
                }
                listener.done(result);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                listener.onFailure();
            }
        });
    }

    /**
     * 列出某貼文的留言。
     *
     * @param postId   貼文的編號（ID）
     * @param listener 處理回傳結果的偵聽器。
     **/
    public void getComment(String postId, final ArticleListener listener) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("comment");
        Query query = myRef.orderByChild("postId").equalTo(postId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<HashMap<String, Object>> commentList = new ArrayList<>();
                for (DataSnapshot subDataSnapshot : dataSnapshot.getChildren()) {
                    HashMap<String, Object> hashMap = (HashMap<String, Object>) subDataSnapshot.getValue();
                    commentList.add(hashMap);
                }
                listener.done(commentList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "取得留言時發生錯誤", databaseError.toException());
                listener.onFailure();
            }
        });
    }

    public void addUserProfileInfo(@Nullable Uri photoUri, @Nullable String displayName
            , final String birthday, final String introduce
            , final UpdateDataListener listener) {
        user = mAuth.getCurrentUser();
        UserProfileChangeRequest.Builder profileUpdates = new UserProfileChangeRequest.Builder();
        if (photoUri != null) {
            profileUpdates.setPhotoUri(photoUri);
        }
        if (!displayName.equals("")) {
            profileUpdates.setDisplayName(displayName);
        }
        user.updateProfile(profileUpdates.build())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            List<? extends UserInfo> profile = user.getProviderData();
                            String reference = "user/" + profile.get(0).getUid() + "/";
                            // Write a message to the database
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            //標題
                            DatabaseReference myRef = database.getReference(reference + "birthday");
                            myRef.setValue(birthday);
                            myRef = database.getReference(reference + "introduce");
                            myRef.setValue(introduce);
                            listener.onSuccess();
                        }
                    }
                });
    }

    public void createAccountByEmail(Activity activity, String email, String password, final UpdateDataListener listener) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "透過電子郵件註冊帳號:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            listener.onSuccess();
                        } else {
                            listener.onFailure();
                        }
                    }
                });
    }

    public void signInByEmail(Activity activity, String email, String password, final UpdateDataListener listener) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                            listener.onSuccess();
                        else {
                            listener.onFailure();
                        }
                    }
                });
    }

    public void addComment(String postId, String comment) {
        String reference = "comment/" + getUid() + "_" + StringHandler.getCurrentDate(StringHandler.FOR_ID_TAG) + "/";
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(reference + "content");
        myRef.setValue(comment);
        myRef = database.getReference(reference + "date");
        myRef.setValue(StringHandler.getCurrentDate(StringHandler.WITH_SPLIT));
        myRef = database.getReference(reference + "uid");
        myRef.setValue(getUid());
        myRef = database.getReference(reference + "postId");
        myRef.setValue(postId);
        myRef = database.getReference(reference + "displayName");
        myRef.setValue(getDisplayName());
    }

    public interface UpdateProfilePhotoListener {
        void onSuccess();

        void onFailure(Exception exception);
    }

    public interface UpdateDataListener {
        void onSuccess();

        void onFailure();
    }

    public interface GetUserInfoListener {
        void done(List<HashMap<String, String>> userInfo, String providerId, String introduce);
    }

    public interface GetAuthorInfoListener {
        void done(String displayName, String userPhotoUrl);
    }

    public interface ArticleListener {
        void done(List<HashMap<String, Object>> listArticle);

        void onFailure();
    }

}
