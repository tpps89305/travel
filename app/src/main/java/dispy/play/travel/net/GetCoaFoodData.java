package dispy.play.travel.net;

import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.object.FoodData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by yangchaofu on 2017/12/5.
 *
 * @author yangchaofu
 * @since 2017/12/5
 */

public class GetCoaFoodData {

    public static void get(final String adminArea, final Listener listener) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://data.coa.gov.tw/Service/OpenData/CC/").build();
        OpenDataAPI openDataAPI = retrofit.create(OpenDataAPI.class);
        Call<ResponseBody> call = openDataAPI.getCoaFoodData();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                List<FoodData> result = new ArrayList<>();
                try {
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    JSONObject jsonObject;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject.optString("address").contains(adminArea)) {
                            FoodData foodData = new FoodData(jsonObject);
                            result.add(foodData);
                        }
                    }
                    listener.done(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(getClass().getSimpleName(), "接收資料時發生錯誤", t);
            }
        });
    }

    public static Marker[] putToMap(GoogleMap map, List<FoodData> foodData) {
        Marker[] result = new Marker[foodData.size()];
        MarkerOptions markerOptions;
        for (int i = 0; i < foodData.size(); i++) {
            markerOptions = new MarkerOptions();
            markerOptions.title(foodData.get(i).getTitle());
            markerOptions.snippet(foodData.get(i).getAddress());
            markerOptions.position(new LatLng(foodData.get(i).getLatitude(), foodData.get(i).getLongitude()));
            result[i] = map.addMarker(markerOptions);
            result[i].setTag(i);
        }
        return result;
    }

    public interface Listener {
        void done(List<FoodData> result);
    }
}
