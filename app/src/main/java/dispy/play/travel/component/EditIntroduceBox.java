package dispy.play.travel.component;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.R;

/**
 * Created by yangchaofu on 2017/8/31.
 */

public class EditIntroduceBox {

    private EditText editIntroduce;

    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    public EditIntroduceBox (Context context){
        LayoutInflater adbInflater = LayoutInflater.from(context);
        View view = adbInflater.inflate(R.layout.dialog_change_introduce, null);

        builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle(R.string.dialog_edit_introduce);
        editIntroduce = view.findViewById(R.id.edit_input);
    }

    public void show(String originIntroduce, final OnDialogDismissListener listener) {
        editIntroduce.setText(originIntroduce);
        builder.setPositiveButton(R.string.btn_submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onClickPositiveButton(editIntroduce.getText().toString());
            }
        });
        if (dialog == null)
            dialog = builder.show();
        else {
            dialog.show();
        }
    }

}
