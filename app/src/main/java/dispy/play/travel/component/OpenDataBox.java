package dispy.play.travel.component;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dispy.play.travel.reference.DataTag;
import dispy.play.travel.R;
import dispy.play.travel.adapter.OpenDataListAdapter;
import dispy.play.travel.main.MoreUserDataList;
import dispy.play.travel.window.OpenDataWindow;

/**
 * Created by tpps8 on 2016/4/27.
 */
public class OpenDataBox extends RelativeLayout {

    private TextView textTitle;
    private Spinner spinnerList;
    private TextView textGOTOMore;
    private TextView textGOTOMoreUserdata;

    private Context context;
    private OpenDataWindow openDataWindow;
    private FragmentInterface fi;

    private boolean isReady;

    public OpenDataBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        inflater.inflate(R.layout.component_opendata_box, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        textTitle = (TextView) findViewById(R.id.text_info_title);
        spinnerList = (Spinner) findViewById(R.id.spinner_opendata_list);
        textGOTOMore = (TextView) findViewById(R.id.text_goto_more);
        textGOTOMoreUserdata = (TextView) findViewById(R.id.text_more_userdata);
        openDataWindow = new OpenDataWindow(context);
    }

    public void setTitle(String title) {
        textTitle.setText(title);
    }

    public void setList(List<HashMap<String, String>> list, double originLatitude, double originLongitude, final DataTag tag) {
        int[] dataTags = new int[3];

        double[] distance = new double[list.size()];
        double temp;
        for (int i = 0; i < list.size(); i++) {
            temp = getDistance(originLatitude,
                    originLongitude,
                    Double.parseDouble(list.get(i).get("latitude")),
                    Double.parseDouble(list.get(i).get("longitude")));
            distance[i] = temp;
        }

        double[] distanceClone = distance.clone();

        for (int i = 0; i < distanceClone.length - 1; i++) {
            for (int j = 0; j < distanceClone.length - i - 1; j++) {
                if (distanceClone[j + 1] < distanceClone[j]) {
                    temp = distanceClone[j + 1];
                    distanceClone[j + 1] = distanceClone[j];
                    distanceClone[j] = temp;
                }
            }
        }

        for (int i = 0; i < dataTags.length; i++) {
            for (int j = 0; j < distance.length; j++) {
                if (distanceClone[i] == distance[j]) {
                    dataTags[i] = j;
                    break;
                }
                else {
                    dataTags[i] = -1;
                }
            }
        }

        initialize(list, dataTags, tag);

    }

    public void setList (List<HashMap<String, String>> list, final DataTag tag) {
        initialize(list, new int[]{0, 1, 2}, tag);
    }

    private double getDistance(double originLatitude, double originLongitude,double destinationLatitude, double destinationLongitude) {
        double radLatitude1 = originLatitude * Math.PI / 180;
        double radLatitude2 = destinationLatitude * Math.PI / 180;
        double l = radLatitude1 - radLatitude2;
        double p = originLongitude * Math.PI / 180 - destinationLongitude * Math.PI / 180;
        double distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(l / 2), 2)
                + Math.cos(radLatitude1) * Math.cos(radLatitude2)
                * Math.pow(Math.sin(p / 2), 2)));
        distance = distance * 6378137.0;
        distance = Math.round(distance * 10000) / 10000;

        return distance ;
    }

    private void initialize(final List<HashMap<String, String>> list, final int[] dataTags, final DataTag tag) {
        fi = (FragmentInterface) context;
        List<HashMap<String, String>> array = new ArrayList<>();
        isReady = false;

        try {
            for(int i = 0; i < dataTags.length; i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("title", list.get(dataTags[i]).get("title"));
                hashMap.put("address", list.get(dataTags[i]).get("address"));
                array.add(hashMap);
            }
        } catch (IndexOutOfBoundsException e) { //處理 list 可能是空的的情況
            Toast.makeText(context, "沒有資料可以顯示", Toast.LENGTH_SHORT).show();
        }

        spinnerList.setAdapter(new OpenDataListAdapter(context, array, R.layout.style_spinner_item));
        spinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Field field;
                Spinner spinner = (Spinner) parent;
                try {
                    field = AdapterView.class.getDeclaredField("mOldSelectedPosition");
                    field.setAccessible(true);
                    field.setInt(spinner, AdapterView.INVALID_POSITION);
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (isReady)
                    openDataWindow.showWindow(parent, list.get(dataTags[position]), tag);
                isReady = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        textGOTOMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tag) {
                    case FOOD:
                        fi.goToDataList(0);
                        break;
                    case CULTURE:
                        fi.goToDataList(1);
                        break;
                    case HISTORICAL_SITES:
                        fi.goToDataList(2);
                        break;
                }

            }
        });

        textGOTOMoreUserdata.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] arrayCategory = getResources().getStringArray(R.array.array_userdata_category);
                Intent intent = new Intent(context, MoreUserDataList.class);
                Bundle bundle = new Bundle();
                switch (tag) {
                    case FOOD:
                        bundle.putString("category", arrayCategory[1]);
                        break;
                    case CULTURE:
                        bundle.putString("category", arrayCategory[2]);
                        break;
                    case HISTORICAL_SITES:
                        bundle.putString("category", arrayCategory[3]);
                        break;
                }
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

}
