package dispy.play.travel.component;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import dispy.play.travel.R;

/**
 * 讀取對話方塊，因應 Progress Dialog 在 Android O 被宣告 deprecated ，採用的新程式。
 *
 * @author dispyTranslate
 * @since 2017-08-25
 * @version 1.0
 */

public class LoadingBox {

    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    public LoadingBox(Context context) {
        LayoutInflater adbInflater = LayoutInflater.from(context);
        View progressLayout = adbInflater.inflate(R.layout.dialog_progress, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(progressLayout);
        builder.setTitle(R.string.dialog_loading);
    }

    public void show() {
        if (dialog == null)
            dialog = builder.show();
        else {
            dialog.show();
        }
    }

    public void dismiss() {
        if (dialog != null)
            dialog.dismiss();
    }
}
