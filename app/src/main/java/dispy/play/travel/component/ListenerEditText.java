package dispy.play.travel.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

/**
 * Created by tpps8 on 2016/4/26.
 */
public class ListenerEditText extends android.support.v7.widget.AppCompatEditText {
    private KeyImeChange keyImeChangeListener;

    public ListenerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setKeyImeChangeListener(KeyImeChange listener) {
        keyImeChangeListener = listener;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        return keyImeChangeListener != null && keyImeChangeListener.onKeyIme(keyCode, event);
    }

    public interface KeyImeChange {
        boolean onKeyIme(int keyCode, KeyEvent event);
    }
}
