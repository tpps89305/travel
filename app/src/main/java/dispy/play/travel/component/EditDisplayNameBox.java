package dispy.play.travel.component;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import dispy.play.travel.R;
import dispy.play.travel.net.FirebaseHandler;

/**
 * Created by yangchaofu on 2017/8/31.
 */

public class EditDisplayNameBox {

    private EditText editDisplayName;

    private AlertDialog.Builder builder;
    private AlertDialog dialog;

    public EditDisplayNameBox (Context context) {
        LayoutInflater adbInflater = LayoutInflater.from(context);
        final View view = adbInflater.inflate(R.layout.dialog_change_name, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle(R.string.dialog_change_name);
        editDisplayName = view.findViewById(R.id.edit_input);
    }

    /**
     * @deprecated 改用新方法
     * **/
    public EditDisplayNameBox show(final OnDialogDismissListener listener) {
        builder.setPositiveButton(R.string.btn_submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onClickPositiveButton(editDisplayName.getText().toString());
            }
        });
        if (dialog == null)
            dialog = builder.show();
        else {
            dialog.show();
        }
        return this;
    }

    public EditDisplayNameBox show(final FirebaseHandler.UpdateDataListener listener) {
        builder.setPositiveButton(R.string.btn_submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseHandler.getInstance().updateDisplayName(editDisplayName.getText().toString(), listener);
            }
        });
        if (dialog == null)
            dialog = builder.show();
        else {
            dialog.show();
        }
        return this;
    }

}
