package dispy.play.travel.component;

/**
 * Created by yangchaofu on 2017/8/31.
 */

public interface OnDialogDismissListener {
    void onClickPositiveButton(String input);
}
