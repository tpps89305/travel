package dispy.play.travel.reference;

/**
 * Created by tpps8 on 2016/4/12.
 */
public enum DataTag {
    FOOD,
    CULTURE,
    HISTORICAL_SITES,
    USER_DATA
}
