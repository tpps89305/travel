package dispy.play.travel.reference;

/**
 * Created by yangchaofu on 2016/1/22.
 */
public class Parameter {
    public static final int WEATHER_DATA_READY = 1;
    public static final int ERROR = -1;
    public static final int REFRESH_DATA = 99;

    public static final String TAG_USER = "user";

    public static final String TAG_LATITUDE = "latitude";
    public static final String TAG_LONGITUDE = "longitude";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_ACCOUNT_NUMBER = "account_no";

}
