package dispy.play.travel;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 字串處理程式。
 *
 * @author Dispy
 * @since 2016-2-16
 * @version 1.1
 */
public class StringHandler {

    public static final int FOR_ID_TAG = 0x01;
    public static final int WITH_SPLIT = 0x02;

    /**
     * 台/臺 字轉換
     *
     * @param context Context 環境
     * @param name 要轉換的名稱（針對有「台」、「臺」字的城市名）
     * @param switchType 要轉換的方式
     * **/
    public static String CityNameSwitch(Context context, String name, int switchType) {

        String[] arrayCityCommonName = context.getResources().getStringArray(R.array.city);
        String[] arrayCityOfficialName = context.getResources().getStringArray(R.array.city2);

        String result = "";
        int i;
        switch (switchType) {
            case 1: //俗名轉正名
                for (i = 0; i < arrayCityCommonName.length; i++) {
                    if (name.equals(arrayCityCommonName[i])) {
                        result = arrayCityOfficialName[i];
                    }
                }
                break;
            case 2: //正名轉俗名
                for (i = 0; i < arrayCityOfficialName.length; i++) {
                    if (name.equals(arrayCityOfficialName[i])) {
                        result = arrayCityCommonName[i];
                    }
                }
                break;
        }
        return result;
    }

    /**
     * 取得現在的時間日期，並依格式化成特定文字。
     *
     * @param type 用途
     * @since 1.1
     * **/
    @SuppressLint("SimpleDateFormat")
    public static String getCurrentDate(int type) {
        //目前時間
        Date date = new Date();
        //設定日期格式
        SimpleDateFormat sdf = null;
        switch (type) {
            case FOR_ID_TAG:
                sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                break;
            case WITH_SPLIT:
                sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                break;
        }
        //進行轉換
        assert sdf != null;
        return sdf.format(date);
    }

    /**
     * 將日期整理成特定格式 YYYY-MM-DD
     *
     * @param year 年
     * @param monthOfYear 月，配合 Date API ，輸入的數字會再加 {@code 1}。
     * @param dayOfMonth 日
     * @since 1.1
     * **/
    public static String toDateFormat(int year,int monthOfYear,int dayOfMonth){
        return String.valueOf(year) + "-"
                + String.valueOf(monthOfYear + 1) + "-"
                + String.valueOf(dayOfMonth);
    }

}
