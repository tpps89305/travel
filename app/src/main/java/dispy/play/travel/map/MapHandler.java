package dispy.play.travel.map;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapHandler {

    public static void drawPath(GoogleMap googleMaps, JSONObject result) {

        try {
            JSONArray routeArray = result.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONArray legsArray = routes.getJSONArray("legs");
            JSONObject legs = legsArray.getJSONObject(0);
            JSONArray stepsArray = legs.getJSONArray("steps");

            //=====顯示每一段路=====

            ArrayList<LatLng> arrayPoints = new ArrayList<LatLng>();
            for (int i = 0; i < stepsArray.length(); i++) {
                JSONObject steps = stepsArray.getJSONObject(i);
                JSONObject polyline = steps.getJSONObject("polyline");
                String points = polyline.getString("points");
                List<LatLng> list = decodePoly(points);

                for (int j = 0; j < list.size(); j++) {
                    LatLng temp = new LatLng(list.get(j).latitude, list.get(j).longitude);
                    arrayPoints.add(temp);
                }
            }
            PolylineOptions lineOptions = new PolylineOptions();
            lineOptions.addAll(arrayPoints);
            lineOptions.width(5);  //導航路徑寬度
            lineOptions.color(Color.BLUE); //導航路徑顏色
            googleMaps.addPolyline(lineOptions);

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public static List<HashMap<String, String>> getStep(JSONObject input) throws JSONException {
        List<HashMap<String, String>> results = new ArrayList<>();
        JSONArray routeArray = input.getJSONArray("routes");
        JSONObject routes = routeArray.getJSONObject(0);
        JSONArray legsArray = routes.getJSONArray("legs");
        JSONObject legs = legsArray.getJSONObject(0);
        JSONArray stepsArray = legs.getJSONArray("steps");

        JSONObject jsonObject, jsonObject2;
        HashMap<String, String> temp;
        for (int i = 0; i < stepsArray.length(); i++) {
            jsonObject = stepsArray.getJSONObject(i);
            temp = new HashMap<>();

            jsonObject2 = jsonObject.getJSONObject("distance");
            temp.put("distance", jsonObject2.getString("text"));
            jsonObject2 = jsonObject.getJSONObject("duration");
            temp.put("duration", jsonObject2.getString("text"));
            temp.put("html_instructions", jsonObject.getString("html_instructions"));
            results.add(temp);
        }
        return results;
    }

    //將路徑碼轉成坐標陣列
    private static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;

    }



}
