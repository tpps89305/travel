package dispy.play.travel.map;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * 位置與定位功能。
 *
 * @version 1.1
 */
public class Localization {

    private static final String TAG = "Localization";
    public static final int PERMISSIONS_REQUEST_LOCATION = 0x001;

    private Context mContext;
    private static Localization instance;

    private Localization(Context context) {
        mContext = context;
    }

    public static Localization getInstance(Context context) {
        if (instance == null) {
            instance = new Localization(context);
        }
        return instance;
    }

    public static Localization getInstance() {
        return instance;
    }

    public static LatLng getMyLocation(Activity activity) {
        LatLng myLatLng = new LatLng(0.0, 0.0);
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE); //LocationManager 提供管道以連接系統區域服務;
        try {
            //取得最佳 provider: 手機或者模擬器上均為 GPS
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);

            int permission = ActivityCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION);

            if (permission == PackageManager.PERMISSION_GRANTED) {
                //取得位置
                Location location = locationManager.getLastKnownLocation(bestProvider);
                myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                Log.i(TAG, "我的位置在 --->" + String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));
            } else {
                Log.e(TAG, "沒有取得位置權限");
            }

        } catch (NullPointerException e) {
            Log.e(TAG, "無法取得位置", e);
            e.printStackTrace();
        }
        return myLatLng;
    }

    public LatLng getMyLocation() {
        LatLng myLatLng = new LatLng(0.0, 0.0);
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE); //LocationManager 提供管道以連接系統區域服務;
        try {
            //取得最佳 provider: 手機或者模擬器上均為 GPS
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);

            int permission = ActivityCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION);

            if (permission == PackageManager.PERMISSION_GRANTED) {
                //取得位置
                Location location = locationManager.getLastKnownLocation(bestProvider);
                myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                Log.i(TAG, "我的位置在 --->" + String.valueOf(location.getLatitude()) + ", " + String.valueOf(location.getLongitude()));
            } else {
                Log.e(TAG, "沒有取得位置權限");
            }

        } catch (NullPointerException e) {
            Log.e(TAG, "無法取得位置", e);
            e.printStackTrace();
        }
        return myLatLng;
    }

    public static String getLocatedCity(Context context, LatLng latLng) {
        String result = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            result = addresses.get(0).getAdminArea();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getLocatedCity(LatLng latLng) {
        String result = "";
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            result = addresses.get(0).getAdminArea();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Address getAddress(Context context, LatLng latLng) {
        Address result;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            result = addresses.get(0);
            Log.d(TAG, "AdminArea: " + addresses.get(0).getAdminArea()); //台北市
            Log.d(TAG, "CountryName: " + result.getCountryName()); //台灣
            Log.d(TAG, "CountryCode: " + addresses.get(0).getCountryCode()); //TW
            Log.d(TAG, "PostalCode: " + addresses.get(0).getPostalCode()); //105
            Log.d(TAG, "Locality: " + addresses.get(0).getLocality()); //松山區
            Log.d(TAG, "SubLocality: " + addresses.get(0).getSubLocality()); //復盛里
            Log.d(TAG, "Thoroughfare: " + addresses.get(0).getThoroughfare()); //光復南路33巷
            Log.d(TAG, "SubThoroughfare: " + addresses.get(0).getSubThoroughfare()); //6
            Log.i(TAG, "Address:" + result); //105台灣台北市松山區光復南路33巷6號
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
