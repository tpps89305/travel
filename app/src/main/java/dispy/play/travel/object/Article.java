package dispy.play.travel.object;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by yangchaofu on 2017/12/8.
 *
 * @author yangchaofu
 * @since 2017/12/8
 */

public class Article implements Serializable {
    private String addressLine;
    private String adminArea;
    private String adminArea_locality;
    private String content;
    private String countryName;
    private String date;
    private String displayName;
    private String locality;
    private double latitude;
    private double longitude;
    private String photo;
    private String uid;
    private String postId;

    public Article(HashMap<String, Object> data) {
        addressLine = String.valueOf(data.get("addressLine"));
        adminArea = String.valueOf(data.get("adminArea"));
        adminArea_locality = String.valueOf(data.get("adminArea_locality"));
        content = String.valueOf(data.get("content"));
        countryName = String.valueOf(data.get("countryName"));
        date = String.valueOf(data.get("date"));
        displayName = String.valueOf(data.get("displayName"));
        locality = String.valueOf(data.get("locality"));
        latitude = (double) data.get("latitude");
        longitude = (double) data.get("longitude");
        photo = String.valueOf(data.get("photo"));
        uid = String.valueOf(data.get("uid"));
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;

    }

    public String getAddressLine() {
        return addressLine;
    }

    public String getAdminArea() {
        return adminArea;
    }

    public String getAdminArea_locality() {
        return adminArea_locality;
    }

    public String getContent() {
        return content;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getDate() {
        return date;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLocality() {
        return locality;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getPhoto() {
        return photo;
    }

    public String getUid() {
        return uid;
    }
}
