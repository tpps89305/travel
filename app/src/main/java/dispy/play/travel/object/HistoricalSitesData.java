package dispy.play.travel.object;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by yangchaofu on 2017/12/5.
 *
 * @author yangchaofu
 * @since 2017/12/5
 */

public class HistoricalSitesData implements Serializable {
    private String name = "";
    private String level = "";
    private String address = "";
    private Double longitude = 0.0;
    private Double latitude = 0.0;
    private String buildingYear = "";
    private String registerDateValue = "";
    private String srcWebsite = "";
    private String adminArea = "";

    public HistoricalSitesData(JSONObject jsonObject) {
        name = jsonObject.optString("name");
        level = jsonObject.optString("level");
        address = jsonObject.optString("address");
        longitude = jsonObject.optDouble("longitude");
        latitude = jsonObject.optDouble("latitude");
        buildingYear = jsonObject.optString("buildingYearName") + " (" + jsonObject.optString("buildingCreateWestYear") + ")";
        registerDateValue = jsonObject.optString("registerDateValue");
        srcWebsite = jsonObject.optString("srcWebsite");
        String[] cityName = jsonObject.optString("cityName").split(" ");
        adminArea = cityName[0];
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    public String getAddress() {
        return address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public String getBuildingYear() {
        return buildingYear;
    }

    public String getRegisterDateValue() {
        return registerDateValue;
    }

    public String getSrcWebsite() {
        return srcWebsite;
    }

    public String getAdminArea() {
        return adminArea;
    }
}
