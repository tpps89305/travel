package dispy.play.travel.object;

public class Weather {

    private String strDateTime;
    private String strCity;
    private String strWeather;
    private String parameterValue;

    public String getStrCity() {
        return strCity;
    }

    public void setStrCity(String title) {
        this.strCity = title;
    }

    public String getStrDateTime() {
        return strDateTime;
    }

    public void setStrDateTime(String strDateTime) {
        this.strDateTime = strDateTime;
    }

    public String getStrWeather() {
        return strWeather;
    }

    public void setStrWeather(String strWeather) {
        this.strWeather = strWeather;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

}
