package dispy.play.travel.object;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by yangchaofu on 2017/12/5.
 *
 * @author yangchaofu
 * @since 2017/12/5
 */

public class FoodData implements Serializable {

    private String title = "";
    private String address = "";
    private String businessHours = "";
    private String description = "";
    private String phone = "";
    private String price = "";
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String route = "";

    public FoodData(JSONObject jsonObject) {
        title = jsonObject.optString("title");
        address = jsonObject.optString("address");
        businessHours = jsonObject.optString("business_hrs");
        description = jsonObject.optString("Description");
        phone = jsonObject.optString("phone");
        price = jsonObject.optString("price");
//        latitude = jsonObject.optDouble("x");
        try {
            latitude = Double.parseDouble(jsonObject.optString("y"));
        } catch (NumberFormatException e) {
            latitude = 0.0;
        }
        try {
            longitude = Double.parseDouble(jsonObject.optString("x"));
        } catch (NumberFormatException e) {
            longitude = 0.0;
        }

//        longitude = jsonObject.optDouble("y");
//        longitude = Double.parseDouble(jsonObject.optString("y"));
        route = jsonObject.optString("route");
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public String getDescription() {
        return description;
    }

    public String getPhone() {
        return phone;
    }

    public String getPrice() {
        return price;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getRoute() {
        return route;
    }
}
