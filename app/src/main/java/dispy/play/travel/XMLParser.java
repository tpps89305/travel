package dispy.play.travel;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import dispy.play.travel.XMLParsingHandler;

public class XMLParser extends DefaultHandler {

    private XMLParsingHandler xmlParsingHandler;
    public XMLParser (XMLParsingHandler parser){
        xmlParsingHandler = parser;
    }

    private Object[] getData(InputStream is) throws SAXException, IOException, ParserConfigurationException {
        Object[] data;
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser sp = spf.newSAXParser();
        XMLReader xr = sp.getXMLReader();

        if(xmlParsingHandler == null)
            throw new NullPointerException("xmlParsingHandle is null");
        else{
            xr.setContentHandler(xmlParsingHandler);
            xr.parse(new InputSource(is));
            data = (Object[]) xmlParsingHandler.getParsedData();
        }
        is.close();
        return data;
    }

    public Object[] getData(String urlPath) throws SAXException, IOException, ParserConfigurationException {
        URL url = new URL(urlPath);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setConnectTimeout(10000);
        uc.connect();
        int status = uc.getResponseCode();
        if(status == HttpURLConnection.HTTP_OK){
            Object[] data = getData(url.openStream());
            return data;
        }
        return null;
    }

}
