package dispy.play.travel.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.StringHandler;
import dispy.play.travel.adapter.SpinnerTagAdapter;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.main.MainActivity;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.net.GetArticle;
import dispy.play.travel.net.GetCoaFoodData;
import dispy.play.travel.net.GetHistoricalSitesData;
import dispy.play.travel.net.NetworkHandler;
import dispy.play.travel.R;
import dispy.play.travel.main.UserDataDetail;
import dispy.play.travel.map.Localization;
import dispy.play.travel.object.Article;
import dispy.play.travel.object.FoodData;
import dispy.play.travel.object.HistoricalSitesData;
import dispy.play.travel.reference.DataTag;
import dispy.play.travel.window.OpenDataWindow;

/**
 * Created by tpps8 on 2016/2/19.
 */
public class DataMap extends BaseFragment implements GoogleMap.OnInfoWindowClickListener, OnMapReadyCallback {

    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;
    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.spinner_location)
    Spinner spinnerLocation;

    private GoogleMap mMap;
    private LatLng userPosition;

    private Activity activity;

    //資料串
    private List<FoodData> foodData;
    private List<HistoricalSitesData> historicalSitesData;
    private List<Article> mUserData;

    private OpenDataWindow openDataWindow;
    private View view;

    private LoadingBox loadingBox;

    private Marker[] mDataMarkers;

    private DataTag dataTag;

    private FirebaseHandler firebaseHandler;

    private String mAdminArea;

    private String[] mArrayCity;

    private int mCategory = 0;
    private boolean isFirstRun = true;
    private boolean isProcessing = false;

    private AdapterView.OnItemSelectedListener selectCategory = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (isFirstRun) {
                String adminArea = Localization.getLocatedCity(activity, userPosition);
                mMap.clear();
                if (!isProcessing && !adminArea.equals(""))
                    selectData(i, adminArea);
                else if (!isProcessing) {
                    selectData(i, "台北市");
                    showToast(R.string.toast_no_location);
                }
                mCategory = i;
                isFirstRun = false;
            } else {
                mMap.clear();
                selectData(i, mAdminArea);
                mCategory = i;
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private AdapterView.OnItemSelectedListener selectLocation = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (!isFirstRun) {
                mMap.clear();
                if (!isProcessing)
                    selectData(mCategory, mArrayCity[i]);
                mAdminArea = mArrayCity[i];
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    /** 選擇想要看到的資訊類型。
     * @param position 下拉式選單的選項位置
     * **/
    private void selectData(int position, String adminArea) {
        isProcessing = true;
        switch (position) {
            case 0:
                loadingBox.show();
                GetCoaFoodData.get(adminArea, new GetCoaFoodData.Listener() {

                    @Override
                    public void done(List<FoodData> result) {
                        foodData = result;
                        mDataMarkers = GetCoaFoodData.putToMap(mMap, foodData);
                        loadingBox.dismiss();
                        isProcessing = false;
                    }
                });
                dataTag = DataTag.FOOD;
                break;
            case 1:
                loadingBox.show();
                GetHistoricalSitesData.get(StringHandler.CityNameSwitch(activity, adminArea, 1),
                        new GetHistoricalSitesData.Listener() {

                    @Override
                    public void done(List<HistoricalSitesData> result) {
                        historicalSitesData = result;
                        mDataMarkers = GetHistoricalSitesData.putToMap(mMap, historicalSitesData);
                        loadingBox.dismiss();
                        isProcessing = false;
                    }
                });
                dataTag = DataTag.HISTORICAL_SITES;
                break;
            case 2:
                getUserData(adminArea);
                dataTag = DataTag.USER_DATA;
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_data_map, container, false);

        ButterKnife.bind(this, view);
        activity = getActivity();

        mapView.onCreate(null);
        mapView.onResume();
        mapView.getMapAsync(this);

        loadingBox = new LoadingBox(activity);
        firebaseHandler = FirebaseHandler.getInstance();

        return view;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        switch (dataTag) {
            case USER_DATA:
                for (int i = 0; i < mUserData.size(); i++) {
                    if (marker.getTag().toString().equals(mUserData.get(i).getPostId())) {
                        Intent intent = new Intent(activity, UserDataDetail.class);
                        intent.putExtra("value", mUserData.get(i));
                        startActivity(intent);
                    }
                }
                break;
            case FOOD:
                String key = marker.getTag().toString();
                int a = Integer.parseInt(key);
                openDataWindow.showFoodData(view, foodData.get(a));
                break;
            case HISTORICAL_SITES:
                String key2 = marker.getTag().toString();
                int a2 = Integer.parseInt(key2);
                openDataWindow.showHSData(view, historicalSitesData.get(a2));
                break;
        }

    }

    private Bitmap resizeMarkerIcon(Bitmap input) {
        return Bitmap.createScaledBitmap(input, input.getWidth() / 2, input.getHeight() / 2, false);
    }

    private Bitmap resizeMarkerIcon(Bitmap input, int scaleExtraLevel) {
        return Bitmap.createScaledBitmap(input, input.getWidth() / (2 * scaleExtraLevel),
                input.getHeight() /  (2 * scaleExtraLevel), false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int permission = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_DATA_MAP);
        } else {
            getLocationData();
        }

    }

    private void markUserPosition() {
        Bitmap markerFood = BitmapFactory.decodeResource(getResources(), R.drawable.marker_person);
        MarkerOptions markerOpt;
        markerOpt = new MarkerOptions();
        markerOpt.position(userPosition)
                .title("我現在在這裡！")
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMarkerIcon(markerFood, 2)));
        mMap.addMarker(markerOpt);
    }

    private void getLocationData() {
        userPosition = Localization.getMyLocation(activity);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPosition, 15), 2000, null);
        mMap.setOnInfoWindowClickListener(this);
        openDataWindow = new OpenDataWindow(getActivity());

        mArrayCity = getResources().getStringArray(R.array.city);
        spinnerLocation.setAdapter(new SpinnerTagAdapter(getActivity(), mArrayCity));
        spinnerLocation.setOnItemSelectedListener(selectLocation);

        String[] arrayDataTag = getResources().getStringArray(R.array.array_spinner_list);
        SpinnerTagAdapter adapter = new SpinnerTagAdapter(getActivity(), arrayDataTag);
        spinnerCategory.setAdapter(adapter);
        spinnerCategory.setOnItemSelectedListener(selectCategory);
    }

    private void getUserData(String adminArea) {
        loadingBox.show();
        GetArticle.get(adminArea, new GetArticle.Listener() {
            @Override
            public void done(List<Article> result) {
                mUserData = result;
                mDataMarkers = GetArticle.putToMap(mMap, mUserData);
                loadingBox.dismiss();
                isProcessing = false;
            }

            @Override
            public void onFailure() {
                showToast(R.string.toast_error_download_post);
                loadingBox.dismiss();
                isProcessing = false;
            }
        });

    }

    public void onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            getLocationData();
        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            Log.e(TAG, "使用者拒絕給予權限");
        }
    }

}
