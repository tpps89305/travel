package dispy.play.travel.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dispy.play.travel.R;
import dispy.play.travel.StringHandler;
import dispy.play.travel.main.Create;
import dispy.play.travel.main.MainActivity;
import dispy.play.travel.main.MoreUserDataList;
import dispy.play.travel.main.SignIn;
import dispy.play.travel.map.Localization;
import dispy.play.travel.net.FirebaseUserInfo;
import dispy.play.travel.net.NetworkHandler;
import dispy.play.travel.reference.Parameter;
import dispy.play.travel.window.WeatherWindow;

/**
 * 首面，顯示各式開放資料（OpenData）
 */
public class InformationPage extends BaseFragment {

    LatLng userPosition;
    //主畫頁的控制項
    //天氣
    @BindView(R.id.image_weather_icon)
    ImageView imageWeatherIcon;
    @BindView(R.id.text_weather_description)
    TextView textWeatherDescription;
    @BindView(R.id.text_weather_temperature)
    TextView textWeatherTemperature;
    @BindView(R.id.text_more_weather)
    TextView textMoreWeather;
    @BindView(R.id.text_more_weather_user_data)
    TextView textMoreWeatherUserData;
    //OpenData 相關
    //天氣
    private List<HashMap<String, String>> weatherData;
    private Bitmap weatherIcon;
    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {

            switch (msg.what) {

                case Parameter.WEATHER_DATA_READY:
                    textWeatherDescription.setText(weatherData.get(0).get("weather") + " " + weatherData.get(0).get("temperature"));
                    imageWeatherIcon.setImageBitmap(weatherIcon);
                    break;

                case Parameter.ERROR:
                    getWeatherData(String.valueOf(msg.obj));
                    break;

            }
        }

    };
    private View.OnClickListener gotoUserDataList = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String[] arrayCategory = getResources().getStringArray(R.array.array_userdata_category);
            Intent intent = new Intent(getActivity(), MoreUserDataList.class);
            Bundle bundle = new Bundle();
            switch (v.getId()) {
                case R.id.text_more_weather_user_data:
                    bundle.putString("category", arrayCategory[0]);
                    break;
            }
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information_page, container, false);
        ButterKnife.bind(this, view);

        textWeatherTemperature.setText("aaaaa");

        int permission = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_INFO_PAGE);
        } else {
            getWeather();
        }

        return view;
    }

    public void getWeather() {
        userPosition = Localization.getInstance().getMyLocation(); //取得使用者現在的位置（緯度 & 經度)
        String address = Localization.getInstance().getLocatedCity(userPosition);
        textWeatherTemperature.setText(address);

        SharedPreferences account = getActivity().getSharedPreferences(Parameter.TAG_USER, 0);
        account.edit().
                putString(Parameter.TAG_LATITUDE, String.valueOf(userPosition.latitude)).
                putString(Parameter.TAG_LONGITUDE, String.valueOf(userPosition.longitude)).
                putString(Parameter.TAG_ADDRESS, address).
                apply();

        //=====取得各式資料=====
        getWeatherData(address);
    }

    @OnClick(R.id.text_more_weather)
    void moreWeather(View v) {
        new WeatherWindow(getActivity()).showWindow(v, weatherData);
    }

    @OnClick(R.id.text_more_weather_user_data)
    void gotoUserDataList() {
        String[] arrayCategory = getResources().getStringArray(R.array.array_userdata_category);
        Intent intent = new Intent(getActivity(), MoreUserDataList.class);
        Bundle bundle = new Bundle();
        bundle.putString("category", arrayCategory[0]);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void getWeatherData(final String adminArea) {

        final String city = StringHandler.CityNameSwitch(getActivity(), adminArea, 1);

        new Thread() {
            public void run() {
                weatherData = NetworkHandler.getWeatherData(city);
                Message message = new Message();
                try {
                    weatherIcon = NetworkHandler.getWeatherIcon(weatherData.get(0).get("icon"));
                    handler.sendEmptyMessage(Parameter.WEATHER_DATA_READY);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    message.what = Parameter.ERROR;
                    message.obj = adminArea;
                    handler.sendMessage(message);
                } catch (IndexOutOfBoundsException e) {
                    handler.sendEmptyMessage(Parameter.ERROR);
                }
            }
        }.start();
    }

    public void onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            getWeather();
        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            Log.e(TAG, "使用者拒絕給予權限");
        }
    }

    @OnClick(R.id.btnCreate)
    void goToCreate() {
        if (FirebaseUserInfo.getInstance().hasUser()) {
            startActivity(new Intent(getActivity(), Create.class));
        } else {
            showToast(R.string.toast_please_login);
            startActivity(new Intent(getActivity(), SignIn.class));
        }
    }

}
