package dispy.play.travel.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dispy.play.travel.adapter.DataListAdapter;
import dispy.play.travel.component.LoadingBox;
import dispy.play.travel.main.MainActivity;
import dispy.play.travel.main.UserDataDetail;
import dispy.play.travel.net.FirebaseHandler;
import dispy.play.travel.net.GetArticle;
import dispy.play.travel.net.GetCoaFoodData;
import dispy.play.travel.net.GetHistoricalSitesData;
import dispy.play.travel.R;
import dispy.play.travel.StringHandler;
import dispy.play.travel.map.Localization;
import dispy.play.travel.object.Article;
import dispy.play.travel.object.FoodData;
import dispy.play.travel.object.HistoricalSitesData;
import dispy.play.travel.reference.DataTag;
import dispy.play.travel.window.OpenDataWindow;

/**
 * 從清單檢視資料。
 *
 * @author dispyTranslate
 * @since 2016/2/24
 * @version 1.1
 */
public class DataList extends BaseFragment {

    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;
    private LoadingBox loadingBox;
    @BindView(R.id.recycler_view_recycler_view)
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    @BindView(R.id.spinner_location)
    Spinner spinnerLocation;

    private Activity activity;
    List<FoodData> foodData; //資料串
    List<Article> userPost;
    List<HistoricalSitesData> historicalSitesData;

    private OpenDataWindow openDataWindow;

    String mAdminArea;
    Address address;

    DataTag tag;

    private FirebaseHandler firebaseHandler;
    private DataListAdapter adapter;

    private int mCategory = 0;
    private boolean isFirstRun = true;
    private boolean isProcessing = false;

    private String[] mArrayCity;

    private AdapterView.OnItemSelectedListener selectCategory = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (isFirstRun) {
                if (!isProcessing && !mAdminArea.equals(""))
                    switchList(i, mAdminArea);
                else if (!isProcessing) {
                    switchList(i, "台北市");
                    showToast(R.string.toast_no_location);
                }
                mCategory = i;
                isFirstRun = false;
            } else {
                switchList(i, mAdminArea);
                mCategory = i;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private AdapterView.OnItemSelectedListener selectLocation = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (!isFirstRun) {
                if (!isProcessing)
                    switchList(mCategory, mArrayCity[i]);
                mAdminArea = mArrayCity[i];
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private DataListAdapter.OnItemClickedListener clickData = new DataListAdapter.OnItemClickedListener() {
        @Override
        public void onItemClicked(View view, int position) {
            switch (tag) {
                case FOOD:
                    openDataWindow.showFoodData(view, foodData.get(position));
                    break;
                case HISTORICAL_SITES:
                    openDataWindow.showHSData(view, historicalSitesData.get(position));
                    break;
                case USER_DATA:
                    Intent intent = new Intent(activity, UserDataDetail.class);
                    intent.putExtra("value", userPost.get(position));
                    startActivity(intent);
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_list, container, false);

        ButterKnife.bind(this, view);

        activity = getActivity();

        int permission = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Localization.PERMISSIONS_REQUEST_LOCATION | MainActivity.REQUEST_FROM_DATA_LIST);
        } else {
            getData();
        }

        openDataWindow = new OpenDataWindow(activity);
        loadingBox = new LoadingBox(activity);
        firebaseHandler = FirebaseHandler.getInstance();

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void getData() {
        LatLng userPosition = Localization.getMyLocation(activity);
        mAdminArea = Localization.getLocatedCity(activity, userPosition);
        address = Localization.getAddress(activity, userPosition);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.array_spinner_list, android.R.layout.simple_dropdown_item_1line);
        spinnerCategory.setAdapter(adapter);
        spinnerCategory.setOnItemSelectedListener(selectCategory);

        mArrayCity = getResources().getStringArray(R.array.city);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(activity,
                R.array.city, android.R.layout.simple_dropdown_item_1line);
        spinnerLocation.setAdapter(adapter2);
        spinnerLocation.setOnItemSelectedListener(selectLocation);

        if (getArguments() != null) {
            spinnerCategory.setSelection(getArguments().getInt("data_tag"));
        }
    }

    private void switchList(int position, String adminArea) {
        isProcessing = true;
        switch (position) {
            case 0:
                loadingBox.show();
                GetCoaFoodData.get(adminArea, new GetCoaFoodData.Listener() {
                    @Override
                    public void done(List<FoodData> result) {
                        foodData = result;
                        List<HashMap<String, String>> list = new ArrayList<>();
                        for (int i = 0 ; i < result.size(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("title", foodData.get(i).getTitle());
                            hashMap.put("content", foodData.get(i).getAddress());
                            list.add(hashMap);
                        }
                        adapter = new DataListAdapter(getActivity(), list);
                        recyclerView.setAdapter(adapter);
                        adapter.setListener(clickData);
                        loadingBox.dismiss();
                        isProcessing = false;
                        tag = DataTag.FOOD;
                    }
                });
                break;
            case 1:
                loadingBox.show();
                GetHistoricalSitesData.get(StringHandler.CityNameSwitch(activity, adminArea, 1),
                        new GetHistoricalSitesData.Listener() {
                    @Override
                    public void done(List<HistoricalSitesData> result) {
                        historicalSitesData = result;
                        List<HashMap<String, String>> list = new ArrayList<>();
                        for (int i = 0; i < result.size(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("title", historicalSitesData.get(i).getName());
                            hashMap.put("content", historicalSitesData.get(i).getAddress());
                            list.add(hashMap);
                        }
                        adapter = new DataListAdapter(getActivity(), list);
                        recyclerView.setAdapter(adapter);
                        adapter.setListener(clickData);
                        loadingBox.dismiss();
                        isProcessing = false;
                        tag = DataTag.HISTORICAL_SITES;
                    }
                });
                break;
            case 2:
                loadingBox.show();
                GetArticle.get(adminArea, new GetArticle.Listener() {
                    @Override
                    public void done(List<Article> result) {
                        userPost = result;
                        List<HashMap<String, String>> toShow = new ArrayList<>();

                        for (int i = 0; i < result.size(); i++) {
                            HashMap<String, String> temp = new HashMap<>();
                            temp.put("title", result.get(i).getContent());
                            temp.put("content", result.get(i).getDisplayName());
                            toShow.add(temp);
                        }
                        adapter = new DataListAdapter(getActivity(), toShow);
                        recyclerView.setAdapter(adapter);
                        adapter.setListener(clickData);
                        tag = DataTag.USER_DATA;
                        loadingBox.dismiss();
                        isProcessing = false;
                    }

                    @Override
                    public void onFailure() {
                        showToast(R.string.toast_error_download_post);
                        loadingBox.dismiss();
                    }
                });
                break;
        }
    }

    public void onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
            getData();
        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            Log.w(TAG, "使用者拒絕給予權限");
        }
    }

}
