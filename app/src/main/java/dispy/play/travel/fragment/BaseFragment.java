package dispy.play.travel.fragment;

import android.app.Fragment;
import android.support.annotation.StringRes;
import android.widget.Toast;

/**
 * Created by yangchaofu on 2017/8/29.
 */

public abstract class BaseFragment extends Fragment {

    public final String TAG = this.getClass().getSimpleName();

    public void showToast(@StringRes int stringRes) {
        Toast.makeText(getActivity(), getResources().getString(stringRes), Toast.LENGTH_SHORT).show();
    }
}
